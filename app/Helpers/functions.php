<?php
function abc(){
    echo 1223;
}
function creatRewite($str) {
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/([s]+)/', '-', $str);
    $str = preg_replace('/ /', '-', $str);
    return $str;
}
function cleanRewriteNoAccent($string)
{
    $string = mb_strtolower($string, "UTF-8");
    $string = convertToUnicode($string);
    $string = removeAccent($string);
    $string = mb_convert_encoding($string, "UTF-8", "UTF-8");
    $string = str_replace("?", "", trim($string));
    $string = str_replace("tr/m2", "triệu một m2", trim($string));
    $string = trim(preg_replace("/[^A-Za-z0-9]/i", " ", $string));
    $string = mb_convert_encoding($string, "UTF-8", "UTF-8");
    $string = trim(preg_replace("/[^A-Za-z0-9]/i", " ", $string));
    $string = removeEmoji($string);
    $string = mb_convert_encoding($string, "UTF-8", "UTF-8");
    $string = str_replace("?", "", trim($string));
    $string = str_replace(" ", "-", trim($string));
    $string = str_replace("--", "-", $string);
    $string = str_replace("--", "-", $string);
    $string = str_replace("--", "-", $string);
    $string = str_replace("--", "-", $string);
    $string = str_replace("--", "-", $string);
    return $string;
}
function crl_remove_empty_tags($string, $replaceTo = null)
{
    // Return if string not given or empty
    if (!is_string($string) || trim($string) == '') return $string;

    // Recursive empty HTML tags
    return preg_replace(
        '/<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:"[^"]*"|"[^"]*"|[\w\-.:]+))?)*\s*/?>\s*</\1\s*>/gixsm',
        !is_string($replaceTo) ? '' : $replaceTo,
        $string
    );
}
