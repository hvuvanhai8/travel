<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banner = Banner::paginate(5);
        $user=Auth::user();
        return view('admin.pages.banner.list',compact('banner','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.banner.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'ban_name' => 'required|min:3|max:255|unique:banner',
            ],
            [
                'ban_name.required' => 'Tên banner không được để trống.',
                'ban_name.min' => 'Tên banner quá ngắn, mời bạn nhập lại!',
                'ban_name.max' => 'Tên banner quá dài, mời bạn nhập lại!',
                'ban_name.unique' => 'Tên banner không được trùng.',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $banner = new Banner();
        $banner->ban_name = $request->input('ban_name');
        
        $banner->ban_active = $request->input('ban_active')=='on'?1:0;

        if ($request->hasFile('ban_picture')) {
            $picture = $request->file('ban_picture');
            $name = 'Img-banner-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/banner');
            $picture->move($destinationPath, $name);
            $banner->ban_picture = $name;
        }
        $notification = array(
            'message' => 'Save news successfully!',
            'alert-type' => 'success'
        );
        if ($banner->save()){
            return redirect('admin/banner')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        //
//        dd(123);
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $banner = Banner::findOrFail($id);
        return view('admin.pages.banner.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'ban_name' => 'required|min:3|max:255',
            ],
            [
                'ban_name.required' => 'Tên banner không được để trống.',
                'ban_name.min' => 'Tên banner quá ngắn, mời bạn nhập lại!',
                'ban_name.max' => 'Tên banner quá dài, mời bạn nhập lại!',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $banner = Banner::findOrFail($id);
        $banner->ban_name = $request->input('ban_name');
        $banner->ban_active =  $request->input('ban_active')=='on'?1:0;

        if ($request->hasFile('ban_picture')) {
            $picture = $request->file('ban_picture');
            $name = 'Img-banner-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/banner');
            $picture->move($destinationPath, $name);
            $banner->ban_picture = $name;
        }
        $notification = array(
            'message' => 'Save news successfully!',
            'alert-type' => 'success'
        );
        if ($banner->save()){
            return redirect('admin/banner')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        dd($id);
        $banner = Banner::FindOrFail($id);
        $banner->delete();

        $notification = array(
            'message' => 'Delete product successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
