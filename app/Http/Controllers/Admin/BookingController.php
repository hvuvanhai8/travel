<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Booking;

class BookingController extends Controller
{
    //
    public function getBooking(){
       
        $book= DB::table('booking')->select('id','book_cus_id','book_total','book_status','book_payment','created_at','updated_at')->where('book_confirm','=',1)->where('book_payment','=',0)->paginate(30);;
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('cus_parent_id','=',0)->get()->toArray();
        // dd($user_book);
        return view('admin.pages.book.list',compact('book','user_book'));
    }
    public function getBookingSuccess(){
        $book= DB::table('booking')->select('id','book_cus_id','book_total','book_status','book_payment','created_at','updated_at')->where('book_payment','=',1)->paginate(30);;
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('cus_parent_id','=',0)->get()->toArray();
        // dd($user_book);
        return view('admin.pages.book.listsucces',compact('book','user_book'));
    }
    public function getdetailBooking($id){
      
        $book= DB::table('booking')->select('id','book_cus_id','book_total','book_status','book_payment','created_at','updated_at')->where('id','=',$id)->get()->first();
        $id_user = $book->book_cus_id;
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('id','=',$id_user)->get()->first();
        $list_cus =  DB::table('customer')->select('id','cus_name','cus_old','cus_gender','cus_email','cus_phone','cus_address','cus_nationlity','cus_passport','cus_date_passport','cus_single_room')->where('cus_parent_id','=',$id_user)->get()->toArray();
        $book_detail = DB::table('booking_detail')->select('id','book_id','book_detail_tour_id','book_detail_quantity','book_detail_status','book_detail_payment')->where('book_id','=',$id)->get()->first();
        $id_tour = $book_detail->book_detail_tour_id;
        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title')->where('id',"=",$id_tour)->first();
        
        // dd($list_cus);
        return view('admin.pages.book.detail',compact('user_book','book','list_cus','tour_detail','book_detail'));
    }

    public function getChangeBookingsuccess($id,$status){

        
        $book = booking::findOrFail($id);
        $book->book_payment = $status;
        $book->save();
    }
}
