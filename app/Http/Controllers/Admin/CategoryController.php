<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category=Category::where('cat_parent_id',0)->paginate(20);
        return view('admin.pages.category.list',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        return view('admin.pages.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = Validator::make(
            $request->all(),
            [
                'cat_name' => 'required|unique:category|min:5|max:255'
            ],
        
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
        
            [
                'cat_name' => 'Tiêu đề',
            ]
        
        );
        if ($validate->fails()) {
            return View('admin.pages.category.add')->withErrors($validate);
        }else{
            $cate = new Category;
            $cate->cat_name = $request->cat_name;
            $cate->cat_type = $request->cat_type;
            $cate->cat_rewrite = creatRewite($request->cat_name);
            $cate->cat_slug = Str::slug($request->cat_name,'-');
            $cate->cat_active = 1;
            $cate->cat_parent_id = 0;
    
            $cate->save();
    
            
            return redirect()->route('category.index')->with(['success'=>' Thêm mới danh mục thành công !']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cat = Category::findOrFail($id);
        return view('admin.pages.category.edit',compact('cat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'cat_name' => 'required|min:5|max:255'
            ],
        
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
        
            [
                'cat_name' => 'Tiêu đề',
            ]
        
        );
        if ($validate->fails()) {
            // dd('exit');
            $cat = Category::findOrFail($id);
            // $cate_child = Category::findOrFail($id);
            return View('admin.pages.category.edit',compact('cat'))->withErrors($validate);
            // return redirect()->back()
        }else{
            $cate = Category::findOrFail($id);
            $cate->cat_name = $request->cat_name;
            $cate->cat_type = $request->cat_type;
            $cate->cat_rewrite = creatRewite($request->cat_name);
            $cate->cat_slug = Str::slug($request->cat_name,'-');
            $cate->cat_active = 1;
            $cate->cat_parent_id = 0;
    
            $cate->save();
    
            
            return redirect()->route('category.index')->with(['success'=>' Cập nhật danh mục thành công !']);
        }
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {
        $cate = Category::findOrFail($id);
        $cate_parent_id = DB::table('category')->select()->where('cat_parent_id',$id)->get();
        $cate_parent_id = Arr::flatten($cate_parent_id);
        if(!empty($cate_parent_id)){
            return redirect()->back()->with(['error'=>' bạn không thể xóa khi danh mục cha có danh mục con !']);
        }else{
            $cate->delete();
            return redirect()->back()->with(['success'=>' Bạn đã xóa thành công !']);
        }
    }


    public function indexCateChild($id)
    {
        
        $cate =  DB::table('category')->select('id','cat_name')->where('id',$id)->get();
        $cate_child = category::where('cat_parent_id',$id)->paginate(20);
        // dd($cate_child->cat_place_id);
        $place = DB::table('place')->select('id','pla_name')->get();
        return view('admin.pages.category.categorychild.list',compact('cate','cate_child','place'));
    }
    public function createCateChild($id)
    {
        
        // $cate_last = DB::table('category')->get();
        // $cate_last = Arr::flatten($cate_last);
        // $last = end($cate_last);
        // dd($last->id);
       
        $cate = DB::table('category')->select('id','cat_name')->where('id',$id)->get();
        $cate = Arr::flatten($cate);
        $place = DB::table('place')->select('id','pla_name')->get();
        return view('admin.pages.category.categorychild.add',compact('cate','place'));
    }
    public function storeCateChild(Request $request,$id){
       
        $validate = Validator::make(
            $request->all(),
            [
                'cat_name' => 'required|unique:category|min:2|max:255'
            ],
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
            [
                'cat_name' => 'Tiêu đề',
            ]
        
        );
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate);
        }else{
            $cat_parent = Category::select('id','cat_name','cat_type','cat_all_child')->where('id',$id)->first();
          
            $cate = new Category;
            $cate->cat_name = $request->cat_name;
            $cate->cat_type = $cat_parent->cat_type;
            $cate->cat_rewrite = creatRewite($request->cat_name);
            $cate->cat_slug = Str::slug($request->cat_name,'-');
            $cate->cat_place_id = $request->cat_place_id;
            $cate->cat_active = 1;
            $cate->cat_parent_id = (int)$id;

            // dd($cate);
            if ($cate->save()){
                if ($cat_parent->cat_all_child==null){
                    $cat_all_child[] = $cate->id;
                    $cat_all_child = implode(',', $cat_all_child);
                }else{
                    $cat_all_child = explode(',', $cat_parent->cat_all_child);
                    $cat_all_child = array_filter($cat_all_child);
                    $cat_all_child[] = $cate->id;
                    $cat_all_child = implode(',', $cat_all_child);
                }

                $cat_parent->cat_all_child = $cat_all_child;
                $cat_parent->save();
                return redirect()->route('catechild.index',$id)->with(['success'=>' Thêm mới danh mục con thành công !']);
            }
            

        }
       
    }
    public function showCateChild($id){
        $cate_child = Category::findOrFail($id);
        $place = DB::table('place')->select('id','pla_name')->get();
        return view('admin.pages.category.categorychild.edit',compact('cate_child','place'));
    }
    public function editCateChild(Request $request,$id){

        $validate = Validator::make(
            $request->all(),
            [
                'cat_name' => 'required|min:2|max:255'
            ],
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
            [
                'cat_name' => 'Tiêu đề',
            ]
        
        );

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }else{
            $cate = Category::findOrFail($id);
            $cat_parent = DB::table('category')->select('id','cat_name','cat_type')->where('id',$id)->first();
            $cate->cat_place_id = $request->cat_place_id;
            $cate->cat_name = $request->cat_name;
            $cate->cat_type = $cat_parent->cat_type;
            $cate->cat_rewrite = creatRewite($request->cat_name);
            $cate->cat_slug = Str::slug($request->cat_name,'-');
            $cate->cat_active = 1;
          
            $cate->save();


            $cate_parent_id = $cate->cat_parent_id;
    
            return redirect()->route('catechild.index',$cate_parent_id)->with(['success'=>' Cập nhật danh mục thành công !']);
        }
    }
    public function deleteCateChild($id){
        $cate = Category::findOrFail($id);
        $cat_parent = Category::select('id','cat_name','cat_type','cat_all_child')->where('id',$cate->cat_parent_id)->first();
        if ($cate->delete()){
            if ($cat_parent->cat_all_child!==null){
                $cat_all_child = explode(',', $cat_parent->cat_all_child);
                $cat_all_child = array_filter($cat_all_child);
                foreach ($cat_all_child as $key=>$cat_item){
                    if($cat_item == $id){
                        unset($cat_all_child[$key]);
                    }
                }
                $cat_all_child = implode(',',$cat_all_child);
                if ($cat_all_child==""){
                    $cat_all_child = null;
                }
                $cat_parent->cat_all_child = $cat_all_child;
                $cat_parent->save();
            }

            return redirect()->back()->with(['success'=>' Bạn đã xóa thành công !']);
        };

    }
    
}
