<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //
    public function index(){

        $tour = DB::table('tour')->select('id','tour_title')->get();
        $sl_tour = count($tour);

        return view('admin.dashborad.dashborad',compact('sl_tour'));
    }
}
