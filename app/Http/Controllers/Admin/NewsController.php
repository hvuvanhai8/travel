<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PHPUnit\Framework\StaticAnalysis\HappyPath\AssertNotInstanceOf\A;
use Validator;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news = News::where('new_active',1)->paginate(30);
        $user=Auth::user();
        $categories = Category::where('cat_active',1)->where('cat_type','tintuc')->get();
        return view('admin.pages.news.list',compact('news','user','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where('cat_active',1)->where('cat_type','tintuc')->get();
        $cat_news_child = null;
        $cat_parent = $categories->groupBy('cat_parent_id');
        if (!empty($cat_parent[0])){
            foreach ($cat_parent[0] as $key=>$cat_item) {
                $arr_cat_child_id = explode(',', $cat_item->cat_all_child);
                $cat_news_child = Category::whereIn('id', $arr_cat_child_id)->where('cat_active', 1)->where('cat_type','tintuc')->get();
            }
        }

        return view('admin.pages.news.add',compact('cat_news_child'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'new_title' => 'required|min:3|max:255|unique:news',
                'new_cat_id' => 'required|numeric',
                'new_description' => 'required|unique:news',
            ],
            [
                'new_title.required' => 'Tin không được để trống.',
                'new_title.min' => 'Tin quá ngắn, mời bạn nhập lại!',
                'new_title.max' => 'Tên tin quá dài, mời bạn nhập lại!',
                'new_title.unique' => 'Tên tin không được trùng.',
                'new_cat_id.required' => 'Mời chọn danh sách tin tức.',
                'new_description.required' => 'Mô tả không được để trống.',
                'new_description.unique' => 'Tên tin không được giống nhau.',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $news = new News();
        $news->new_title = $request->input('new_title');
        $news->new_cat_id = $request->input('new_cat_id');
        $news->new_user_id  = Auth::user()->id;
        $news->new_active = 1;
        $news->new_slug = Str::slug($request->input('new_title'));
        $news->new_description = $request->input('new_description');
        $news->new_teaser = Str::limit(strip_tags(html_entity_decode($request->input('new_description'))),300,'...');

        if ($request->hasFile('new_picture')) {
            $picture = $request->file('new_picture');
            $name = 'Img-news-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/news');
            $picture->move($destinationPath, $name);
            $news->new_picture = $name;
        }
        $notification = array(
            'message' => 'Save news successfully!',
            'alert-type' => 'success'
        );
        if ($news->save()){
            return redirect('admin/news')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $news = News::findOrFail($id);
        $categories = Category::where('cat_active',1)->where('cat_type','tintuc')->get();
        $cat_news_child = null;
        $cat_parent = $categories->groupBy('cat_parent_id');
        if (!empty($cat_parent[0])){
            foreach ($cat_parent[0] as $key=>$cat_item) {
                $arr_cat_child_id = explode(',', $cat_item->cat_all_child);
                $cat_news_child = Category::whereIn('id', $arr_cat_child_id)->where('cat_active', 1)->where('cat_type','tintuc')->get();
            }
        }
        return view('admin.pages.news.edit',compact('news','cat_news_child'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $news = News::FindOrFail($id);
        $validator = Validator::make($request->all(),
            [
                'new_title' => 'required|min:3|max:255',
                'new_cat_id' => 'required|numeric',
                'new_description' => 'required',
            ],
            [
                'new_title.required' => 'Tin không được để trống.',
                'new_title.min' => 'Tin quá ngắn, mời bạn nhập lại!',
                'new_title.max' => 'Tên tin quá dài, mời bạn nhập lại!',
                'new_cat_id.required' => 'Mời chọn danh sách tin tức.',
                'new_description.required' => 'Mô tả không được để trống.',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $news->new_title = $request->input('new_title');
        $news->new_cat_id = $request->input('new_cat_id');
        $news->new_user_id  = Auth::user()->id;
        $news->new_active = 1;
        $news->new_slug = Str::slug($request->input('new_title'));
        $news->new_description = $request->input('new_description');
        $news->new_teaser = Str::limit(strip_tags(html_entity_decode($request->input('new_description'))),300,'...');

        if ($request->hasFile('new_picture')) {
            $picture = $request->file('new_picture');
            $name = 'Img-news-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/news');
            $picture->move($destinationPath, $name);
            $news->new_picture = $name;
        }
        $notification = array(
            'message' => 'Save news successfully!',
            'alert-type' => 'success'
        );
        if ($news->save()){
            return redirect('admin/news')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
