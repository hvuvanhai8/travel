<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $places_tn = Place::where('pla_active',1)->where('pla_type',1)->get();
        $places_nn = Place::where('pla_active',1)->where('pla_type',0)->get();

        return view('admin.pages.place.list',compact('places_tn','places_nn'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.place.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'pla_name' => 'required|min:2|max:255|unique:place',
                'pla_description' => 'required',
            ],
            [
                'pla_name.required' => 'Tên địa chỉ không được để trống.',
                'pla_name.min' => 'Tên địa chỉ quá ngắn, mời bạn nhập lại!',
                'pla_name.max' => 'Tên địa chỉ quá dài, mời bạn nhập lại!',
                'pla_name.unique' => 'Tên địa chỉ không được trùng.',
                'pla_description.required' => 'Mô tả không được để trống.',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $places = new Place();
        $places->pla_name = $request->input('pla_name');
        $places->pla_code = $request->input('pla_code');
        $places->pla_type = $request->input('pla_type');
        $places->pla_active = 1;
        $places->pla_slug = Str::slug($request->input('pla_name'));
        $places->pla_description = $request->input('pla_description');

        if ($request->hasFile('pla_photo')) {
            $picture = $request->file('pla_photo');
            $name = 'Img-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/places');
            $picture->move($destinationPath, $name);
            $places->pla_picture = $name;
        }
        $notification = array(
            'message' => 'Save product successfully!',
            'alert-type' => 'success'
        );
        if ($places->save()){
            return redirect('admin/place')->with($notification);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        dd(1+$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $place = Place::findOrFail($id);
        return view('admin.pages.place.edit',compact('place'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $place = Place::FindOrFail($id);
        $validator = Validator::make($request->all(),
            [
                'pla_name' => 'required|min:2|max:255',
                'pla_description' => 'required',
            ],
            [
                'pla_name.required' => 'Tên địa chỉ không được để trống.',
                'pla_name.min' => 'Tên địa chỉ quá ngắn, mời bạn nhập lại!',
                'pla_name.max' => 'Tên địa chỉ quá dài, mời bạn nhập lại!',
                'pla_description.required' => 'Mô tả không được để trống.',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $place->pla_name = $request->input('pla_name');
        $place->pla_code = $request->input('pla_code');
        $place->pla_type = $request->input('pla_type');
        $place->pla_active = 1;
        $place->pla_slug = Str::slug($request->input('pla_name'));
        $place->pla_description = $request->input('pla_description');

        if ($request->hasFile('pla_photo')) {
            $picture = $request->file('pla_photo');
            $name = 'Img-'.time().'.'.$picture->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/places');
            $picture->move($destinationPath, $name);
            $place->pla_picture = $name;
        }
        $notification = array(
            'message' => 'Save product successfully!',
            'alert-type' => 'success'
        );
        if ($place->save()){
            return redirect('admin/place')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
