<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Expediency;
use App\Models\Place;
use App\Models\Tour;
use App\User;
use Auth;
use Validator;



class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        

        $tour=Tour::paginate(10);
        $expediency = DB::table('expediency')->select('id','exp_name')->get();
        $users = DB::table('users')->select('id','name')->get();
        $cate = DB::table('category')->select('id','cat_name')->get();
        $place = DB::table('place')->select('id','pla_name')->get();
        // dd($expediency);
        // dd($tour);

        return view('admin.pages.tour.list',compact('tour','expediency','users','cate','place'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();
        $expediency = Expediency::all();
        $place = DB::table('place')->select('id','pla_name')->get();
        $cate_child = DB::table('category')->select('id','cat_name','cat_parent_id')->where('cat_parent_id','!=',0)->get();
        // $cate_parent = DB::table('category')->select('id','cat_name','cat_parent_id')->where('cat_parent_id','=',0)->get();
        
        // $cate_child = Arr::flatten($cate_child);
       
        // foreach($cate_child as $cat_c){
        //     $h = $cat_c->cat_parent_id;
            
        // }
       
    
        return view('admin.pages.tour.add',compact('expediency','user','place','cate_child'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = Validator::make(
            $request->all(),
            [
                'tour_title' => 'required|min:5|max:255',
                'tour_code' => 'required|min:3',
                'tour_price' => 'required',
                'tour_blank' =>'required',
                'tour_from' => 'required',
                'tour_date_go' => 'required',
                'tour_date_back' => 'required',
                'tour_time' => 'required',
            ],
        
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
        
            [
                'tour_title' => 'Tiêu đề',
                'tour_code' => 'Mã tour',
                'tour_price' => 'Giá tour người lớn',
                'tour_blank' => 'Số chỗ trống',
                'tour_from' => 'Khởi hành hành',
                'tour_date_go' => 'Ngày đi',
                'tour_date_back' => 'Ngày về',
                'tour_time' => 'Mô tả thời gian'
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }else{
            $username = Auth::user()->id;
            // dd($request->tour_price_child);
            $tour = new Tour;
            $tour->tour_title = $request->tour_title;
            $tour->tour_code = $request->tour_code;
            $tour->tour_cat_id = $request->tour_cat_id;
            $tour->tour_user_id = $username;
            $tour->tour_pla_id = $request->tour_pla_id;
            $tour->tour_type = $request->tour_type;
            $tour->tour_expediency = $request->tour_expediency;
            $tour->tour_price = $request->tour_price;
            if($request->tour_price_child !== null){
                $tour->tour_price_child = $request->tour_price_child;
            }else{
                $tour->tour_price_child = 0;
            }
            if($request->tour_price_baby !== null){
                $tour->tour_price_baby = $request->tour_price_baby;
            }else{
                $tour->tour_price_baby = 0;
            }
            if($request->tour_price_foreign !== null){
                $tour->tour_price_foreign = $request->tour_price_foreign;
            }else{
                $tour->tour_price_foreign = 0;
            }
            if($request->tour_price_child_foreign !== null){
                $tour->tour_price_child_foreign = $request->tour_price_child_foreign;
            }else{
                $tour->tour_price_child_foreign = 0;
            }
            if($request->tour_price_baby_foreign !== null){
                $tour->tour_price_baby_foreign = $request->tour_price_baby_foreign;
            }else{
                $tour->tour_price_baby_foreign = 0;
            }
            if($request->tour_price_single_room !== null){
                $tour->tour_price_single_room = $request->tour_price_single_room;
            }else{
                $tour->tour_price_single_room = 0;
            }
            $tour->tour_blank = $request->tour_blank;
            $tour->tour_slug = Str::slug($request->tour_title,'-');
            $tour->tour_from = $request->tour_from;
            $tour->tour_status = $request->tour_status;
            if($request->tour_price_pro !== null){
                $tour->tour_price_pro = $request->tour_price_pro;
                $price = $request->tour_price;
                $price = (int)str_replace(",","",$price);
                $price_pro = $request->tour_price_pro;
                $price_pro = (int)str_replace(",","",$price_pro);
                $tour->tour_price_discount = "-".number_format($price - $price_pro);
            }else{
                $tour->tour_price_pro = 0;
                $tour->tour_price_discount = 0;
            }
            if($request->tour_hot !== null){
                $tour->tour_hot = $request->tour_hot;
            }else{
                $tour->tour_hot = 0;
            }
            $tour->tour_date_go = $request->tour_date_go;
            $tour->tour_date_back = $request->tour_date_back;
            $tour->tour_time = $request->tour_time;
            $tour->tour_description = $request->tour_description;

            if ($request->hasFile('tour_picture')) {
                $picture = $request->file('tour_picture');
                $name = 'Img-'.time().'.'.$picture->getClientOriginalExtension();
                $destinationPath = public_path('/uploadfile/tour');
                $picture->move($destinationPath, $name);
                $tour->tour_picture = $name;
            }

            $tour->save();

            return redirect()->route('tour.index')->with(['success'=>' Thêm mới tour thành công ']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::all();
        $expediency = Expediency::all();
        $place = DB::table('place')->select('id','pla_name')->get();
        $category = DB::table('category')->select('id','cat_name')->where('cat_parent_id','!=',0)->get();
        $tour = tour::findOrFail($id);
        // dd($tour);
        

        return view('admin.pages.tour.edit',compact('tour','expediency','user','place','category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateTour(Request $request, $id)
    {
        //
        $validate = Validator::make(
            $request->all(),
            [
                'tour_title' => 'required|min:5|max:255',
                'tour_code' => 'required|min:3',
                'tour_price' => 'required',
                'tour_blank' =>'required',
                'tour_from' => 'required',
                'tour_date_go' => 'required',
                'tour_date_back' => 'required',
                'tour_time' => 'required',
            ],
        
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],
        
            [
                'tour_title' => 'Tiêu đề',
                'tour_code' => 'Mã tour',
                'tour_price' => 'Giá tour người lớn',
                'tour_blank' => 'Số chỗ trống',
                'tour_from' => 'Khởi hành hành',
                'tour_date_go' => 'Ngày đi',
                'tour_date_back' => 'Ngày về',
                'tour_time' => 'Mô tả thời gian'
            ]
        );

        if ($validate->fails()) {
            return redirect()->route('tour.create')->withErrors($validate);
        }else{
            $tour = tour::findOrFail($id);
            $tour->tour_title = $request->tour_title;
            $tour->tour_code = $request->tour_code;
            $tour->tour_cat_id = $request->tour_cat_id;
            $tour->tour_pla_id = $request->tour_pla_id;
            $tour->tour_type = $request->tour_type;
            $tour->tour_expediency = $request->tour_expediency;
            $tour->tour_price = $request->tour_price;
            if($request->tour_price_child !== 0 || $request->tour_price_child !== null){
                $tour->tour_price_child = $request->tour_price_child;
            }else{
                $tour->tour_price_child = 0;
            }
            if($request->tour_price_baby !== 0 || $request->tour_price_baby !== null){
                $tour->tour_price_baby = $request->tour_price_baby;
            }else{
                $tour->tour_price_baby = 0;
            }
            if($request->tour_price_foreign !== 0 || $request->tour_price_foreign !== null){
                $tour->tour_price_foreign = $request->tour_price_foreign;
            }else{
                $tour->tour_price_foreign = 0;
            }
            if($request->tour_price_child_foreign !== 0 || $request->tour_price_child_foreign !== null){
                $tour->tour_price_child_foreign = $request->tour_price_child_foreign;
            }else{
                $tour->tour_price_child_foreign = 0;
            }
            if($request->tour_price_baby_foreign !== 0 || $request->tour_price_baby_foreign !== null){
                $tour->tour_price_baby_foreign = $request->tour_price_baby_foreign;
            }else{
                $tour->tour_price_baby_foreign = 0;
            }
            if($request->tour_price_single_room !== 0 || $request->tour_price_single_room !== null){
                $tour->tour_price_single_room = $request->tour_price_single_room;
            }else{
                $tour->tour_price_single_room = 0;
            }
            $tour->tour_blank = $request->tour_blank;
            $tour->tour_slug = Str::slug($request->tour_title,'-');
            $tour->tour_from = $request->tour_from;
            $tour->tour_status = $request->tour_status;
            if($request->tour_price_pro !== 0){
                $tour->tour_price_pro = $request->tour_price_pro;
                $price = $request->tour_price;
                $price = (int)str_replace(",","",$price);
                $price_pro = $request->tour_price_pro;
                $price_pro = (int)str_replace(",","",$price_pro);
                $tour->tour_price_discount = "-".number_format($price - $price_pro);
            }
            if($request->tour_price_pro == 0){
                $tour->tour_price_pro = $request->tour_price_pro;
                $tour->tour_price_discount = 0;
            }
            if($request->tour_hot !== null){
                $tour->tour_hot = $request->tour_hot;
            }else{
                $tour->tour_hot = 0;
            }
            $tour->tour_date_go = $request->tour_date_go;
            $tour->tour_date_back = $request->tour_date_back;
            $tour->tour_time = $request->tour_time;
            $tour->tour_description = $request->tour_description;

            if ($request->hasFile('tour_picture')) {
                $picture = $request->file('tour_picture');
                $name = 'Img-'.time().'.'.$picture->getClientOriginalExtension();
                $destinationPath = public_path('/uploadfile/tour');
                $picture->move($destinationPath, $name);
                $tour->tour_picture = $name;
            }

            $tour->save();

            return redirect()->route('tour.index')->with(['success'=>' Sửa tour thành công ']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deleteTour($id)
    {
        //
        $tour = tour::findOrFail($id);
        $tour->delete();
        return redirect()->back()->with(['success'=>' Bạn đã xóa tour thành công !']);
    }
}
