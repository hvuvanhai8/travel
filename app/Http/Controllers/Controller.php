<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // share biến cate ra tất cả các view

    	$cate_p = DB::table('category')->select('id','cat_name','cat_parent_id','cat_slug','cat_type')->where('cat_parent_id',"=",0)->get();
        $cate_c = DB::table('category')->select('id','cat_name','cat_parent_id','cat_slug','cat_type')->where('cat_parent_id',"!=",0)->get();
        $place = DB::table('place')->select('id','pla_name')->where('pla_type','=',1)->get();

        $cat_tn = DB::table('category')->select('id','cat_name','cat_rewrite','cat_slug','cat_type')->where('cat_type',"=",'trongnuoc')->where('cat_parent_id','!=',0)->limit(21)->get();
        $cat_nn = DB::table('category')->select('id','cat_name','cat_rewrite','cat_slug','cat_type')->where('cat_type',"=",'nuocngoai')->where('cat_parent_id','!=',0)->limit(21)->get();


        View::share('cate_p', $cate_p);
        View::share('cate_c', $cate_c);
        View::share('place', $place);
        View::share('cat_tn', $cat_tn);
        View::share('cat_nn', $cat_nn);
    }

    function getParentCategory(){
        $category = Category::all();
        $cat_parent = $category->groupBy('cat_parent_id');
        if (isset($cat_parent[0])){
            foreach ($cat_parent[0] as $key=>$cat_item){
                $cat_parent[0][$key]->cat_child = isset($cat_parent[$cat_item->cat_id])?$cat_parent[$cat_item->cat_id]:[];
            }
        }
        return $cat_parent;
    }
}
