<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Customer;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\BookingDetail;
use Validator;

class BookController extends Controller
{
    //
    public function getBooking($id){

        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_price_child','tour_price_baby','tour_price_foreign','tour_price_child_foreign','tour_price_baby_foreign','tour_price_single_room','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id)->where('tour_status',"=",1)->first();
        $expediency = DB::table('expediency')->select('id','exp_name')->get();

        return view('web.pages.book_tour.book',compact('tour_detail','expediency'));
    }

    public function postBooking1(Request $request,$id){

        $validate = Validator::make(
            $request->all(),
            [
                'cus_name' => 'required|min:2|max:255',
                'cus_phone' => 'required',
                'cus_email' => 'required',
                'cus_address' => 'required',
            ],
            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
            ],
            [
                'cus_name' => 'Họ tên',
                'cus_phone' => 'Điện thoại',
                'cus_email' => 'Email',
                'cus_address' => 'Địa chỉ'
            ]
        
        );

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }else{
            $id_tour = $id;
            $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_price_child','tour_price_baby','tour_price_foreign','tour_price_child_foreign','tour_price_baby_foreign','tour_price_single_room','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id_tour)->first();

            
            $customer = new Customer;
            $customer->cus_parent_id = 0;
            $customer->cus_name = $request->cus_name;
            $customer->cus_phone = $request->cus_phone;
            $customer->cus_email = $request->cus_email;
            $customer->cus_address = $request->cus_address;

            if($customer->save()){
                $id_cus = $customer->id;
                $booking = new Booking;
                $booking->book_cus_id = $id_cus;
                $booking->book_confirm = 0;
                $booking->book_status = 0;
                $booking->book_payment = 0;
               

                if($booking->save()){
                    $id_book = $booking->id;
                    $bookingdetail = new BookingDetail;
                    $bookingdetail->book_id = $id_book;
                    $bookingdetail->book_detail_tour_id = $id_tour;
                    $bookingdetail->book_detail_quantity = $request->tong_nguoidi;
                    $bookingdetail->book_detail_status = 0;
                    $bookingdetail->book_detail_payment = 0;
                    $bookingdetail->save();
                }
            }

            $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('id','=',$id_cus)->first();

            
            
            $total_people = $request->tong_nguoidi;
        
            // dd($user_book);
            return view('web.pages.book_tour.bookdetail',compact('user_book','total_people','tour_detail','id_book'));
        }
    }

    public function postBooking2(Request $request,$id_user,$total,$id_book,$id_tour,$slug){

        // dd($request->cus_name2);
        
        $cus_del = DB::table('customer')->where('cus_parent_id','=',$id_user)->delete();
        // $cus_del -> delete();
        // dd($cus_del);

        for($i = 1; $i <=$total; $i++){
            $customer = new Customer;
            $customer->cus_parent_id = $id_user;
            $customer->cus_name = $request->input('cus_name'.$i);
            $customer->cus_gender = $request->input('cus_gender'.$i);
            $customer->cus_old = $request->input('cus_old'.$i);
            $customer->cus_passport = $request->input('cus_passport'.$i);
            $customer->cus_date_passport = $request->input('cus_date_passport'.$i);
            $customer->cus_nationlity = $request->input('cus_nationlity'.$i);
            $customer->cus_single_room = $request->input('cus_single_room'.$i);

            $customer->save();
        }
       
        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_price_child','tour_price_baby','tour_price_foreign','tour_price_child_foreign','tour_price_baby_foreign','tour_price_single_room','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id_tour)->first();
        $list_cus =  DB::table('customer')->select('id','cus_name','cus_old','cus_gender','cus_email','cus_phone','cus_address','cus_nationlity')->where('cus_parent_id','=',$id_user)->get()->toArray();
        
        // dd($list_cus);

        $price_nl = $tour_detail->tour_price;
        $price_nl = (int)str_replace(",","",$price_nl);

        $price_te = $tour_detail->tour_price_child;
        $price_te = (int)str_replace(",","",$price_te);

        $price_eb = $tour_detail->tour_price_baby;
        $price_eb = (int)str_replace(",","",$price_eb);

        $price_nl_nn = $tour_detail->tour_price_foreign;
        $price_nl_nn = (int)str_replace(",","",$price_nl_nn);

        $price_te_nn = $tour_detail->tour_price_child_foreign;
        $price_te_nn = (int)str_replace(",","",$price_te_nn);

        $price_eb_nn = $tour_detail->tour_price_baby_foreign;
        $price_eb_nn = (int)str_replace(",","",$price_eb_nn);

        // dd($price_eb_nn);
        

        $total_bill = (int)$total*$price_nl;

        // dd($total_bill); 
        
        $book = booking::findOrFail($id_book);
        $book->book_total = $total_bill;
        $book->save();

        // dd($book->book_total);

        $expediency = DB::table('expediency')->select('id','exp_name')->get();
       
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('id','=',$id_user)->first();

        // dd($price_nl);

        return redirect()->route('bookview',[$id_tour,$id_user,$id_book]);
    }

    public function getviewBooking2($id_tour,$id_user,$id_book){
        // dd($id_user);
        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_price_child','tour_price_baby','tour_price_foreign','tour_price_child_foreign','tour_price_baby_foreign','tour_price_single_room','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id_tour)->first();
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('id','=',$id_user)->first();
        $expediency = DB::table('expediency')->select('id','exp_name')->get();
        $list_cus =  DB::table('customer')->select('id','cus_name','cus_old','cus_gender','cus_email','cus_phone','cus_address')->where('cus_parent_id','=',$id_user)->get()->toArray();
        $book= DB::table('booking')->select('id','book_cus_id','book_total','book_status','book_payment')->where('id','=',$id_book)->get()->first();
        // dd($book);

        return view('web.pages.book_tour.bookconfirm',compact('user_book','tour_detail','expediency','list_cus','book'));
    }

    public function postConfirm1(Request $request,$id_book){
        
       
       
        $book = booking::findOrFail($id_book);
        $book->book_confirm = 1;
        $book->save();

        $book_detail = DB::table('booking_detail')->select('id','book_id','book_detail_tour_id','book_detail_quantity','book_detail_status','book_detail_payment')->where('book_id','=',$id_book)->get()->first();
        
        
        $sl_nguoidat = $book_detail->book_detail_quantity;
        $id_tour = $book_detail->book_detail_tour_id;
        
        $tour_detail = tour::findOrFail($id_tour);
        $tour_blank_new = $tour_detail->tour_blank - $sl_nguoidat;
        $tour_detail->tour_blank = $tour_blank_new;
        $tour_detail->save();
        
        
        
        // dd($user_book);
        return redirect()->route('confirm2',$id_book);
    }
    public function postConfirm2(Request $request,$id_book){
       
        $book= DB::table('booking')->select('id','book_cus_id','book_total','book_status','book_payment','created_at','updated_at')->where('id','=',$id_book)->get()->first();
        
        $book_detail = DB::table('booking_detail')->select('id','book_id','book_detail_tour_id','book_detail_quantity','book_detail_status','book_detail_payment')->where('book_id','=',$id_book)->get()->first();
        // dd($book_detail);
        $id_user = $book->book_cus_id;
        $user_book = DB::table('customer')->select('id','cus_name','cus_email','cus_phone','cus_address')->where('id','=',$id_user)->first();
        $list_cus =  DB::table('customer')->select('id','cus_name','cus_old','cus_gender','cus_email','cus_phone','cus_address','cus_nationlity')->where('cus_parent_id','=',$id_user)->get()->toArray();
        $expediency = DB::table('expediency')->select('id','exp_name')->get();
        
        $id_tour = $book_detail->book_detail_tour_id;
        
        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_price_child','tour_price_baby','tour_price_foreign','tour_price_child_foreign','tour_price_baby_foreign','tour_price_single_room','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id_tour)->first();

        
        return view('web.pages.book_tour.booksuccess',compact('user_book','list_cus','tour_detail','expediency','book'));
    }
}


