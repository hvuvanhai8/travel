<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\Category;
use App\Models\Tour;
use App\Models\Place;
use App\Mail\SendMail;
use Mail;

class HomeController extends Controller
{
    //
    public function getTest(Request $request){
        $emails = $data['emails'] ?? 'haivv32@wru.vn';
        Mail::to($emails)->send(new SendMail(['emails' => $emails]));

        $this->response['data'] = [];
        $this->response['message'] = '';
        $this->response['status'] = 1;
        return response()->json($this->response);

//        dd(1);
//        return view('welcome');
    }

    public function getHome(){

        $tour_hot = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('tour_hot',"=",1)->where('tour_status',"=",1)->limit(8)->get();
        $tour_tn = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('tour_type',"=",1)->where('tour_status',"=",1)->limit(4)->get();
        $tour_nn = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('tour_type',"=",0)->where('tour_status',"=",1)->limit(4)->get();
        $banner = Banner::where('ban_active',1)->limit(3)->orderBy('id','asc')->get();
        $categories_news = Category::where('cat_active',1)->where('cat_type','tintuc')->get();
        $cat_news_child = null;
        $cat_parent = $categories_news->groupBy('cat_parent_id');

        if (!empty($cat_parent[0])){
            foreach ($cat_parent[0] as $key=>$cat_item) {
                $arr_cat_child_id = explode(',', $cat_item->cat_all_child);
                $cat_news_child = Category::whereIn('id', $arr_cat_child_id)->where('cat_active', 1)->where('cat_type','tintuc')->get();
            }
        }
        foreach ($cat_news_child as $key=>$cat_item){
            $news = News::where('new_active',1)->where('new_cat_id',$cat_item->id)->orderBy('id','desc')->limit(4)->get();
            $cat_news_child[$key]->news = $news;
            $cat_news_child[$key]->first_new = $news->first();
        }
        return view('web.pages.home.home',compact('tour_hot','tour_tn','tour_nn','banner','cat_news_child'));
    }

    public function getCategoryParent($id){
        if(isset($_COOKIE['id_tour'])){
            $cookie = $_COOKIE['id_tour'];
            $list_id = explode(",", $cookie);
            $seen_tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_picture','tour_blank','tour_status')->whereIn('id',$list_id)->where('tour_status',"=",1)->limit(6)->get();
        }else{
            $seen_tour = [];
        }

        $cat_parent = DB::table('category')->select('id','cat_name','cat_slug','cat_type')->where('id','=',$id)->first();
        $cate_child = DB::table('category')->select()->where('cat_parent_id',"=",$id)->get()->toArray();
        $cat_type = (current($cate_child)->cat_type);
        foreach($cate_child as $key => $cat){
                $cate_child_id[$key] = $cat->id;
        }
        foreach( $cate_child_id as $key => $cat )
        {
            $cate_child_id[$key] = $cat;
        }
        // dd($cate_child_id);
        $tour_tn = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->whereIn('tour_cat_id',$cate_child_id)->where('tour_status',"=",1)->limit(6)->get();
        $cate_child = Arr::random($cate_child, 8);
        return view('web.pages.list_tour.listParent',compact('tour_tn','cate_child','cat_type','cat_parent','seen_tour'));
    }
    public function getTourDoanhNghiep(){
        // dd(123);
        if(isset($_COOKIE['id_tour'])){
            $cookie = $_COOKIE['id_tour'];
            $list_id = explode(",", $cookie);
            $seen_tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_picture','tour_blank','tour_status')->whereIn('id',$list_id)->where('tour_status',"=",1)->limit(6)->get();
        }else{
            $seen_tour = [];
        }

        return view('web.pages.enterprise.enterprise',compact('seen_tour'));
    }
    public function getCategoryChild($id){
        if(isset($_COOKIE['id_tour'])){
            $cookie = $_COOKIE['id_tour'];
            $list_id = explode(",", $cookie);
            $seen_tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_picture','tour_blank','tour_status')->whereIn('id',$list_id)->where('tour_status',"=",1)->limit(6)->get();
        }else{
            $seen_tour = [];
        }

        $cat_parent_id = DB::table('category')->select('id','cat_name','cat_place_id','cat_slug','cat_type','cat_parent_id')->where('id','=',$id)->first();
        $id_parent = $cat_parent_id->cat_parent_id;
        $id_place = $cat_parent_id->cat_place_id;
        $cat_parent = DB::table('category')->select('id','cat_name','cat_slug','cat_type','cat_parent_id')->where('id','=',$id_parent)->first();
        $cate_right = DB::table('category')->select()->where('cat_parent_id',"=",$id_parent)->get()->toArray();

        $place_list = DB::table('place')->select('id','pla_name','pla_type','pla_picture','pla_description','pla_active')->where('id',"=",$id_place)->get()->toArray();
        $place_list = current($place_list);
        // $place = current($place);

        // dd($place);
        $cate_right = Arr::random($cate_right, 8);
        $tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('tour_cat_id',"=",$id)->where('tour_status',"=",1)->get();

        return view('web.pages.list_tour.list',compact('tour','cate_right','cat_parent_id','cat_parent','place_list','seen_tour'));
    }

    public function getTourdetail($id){

        if(isset($_COOKIE['id_tour'])){
            setcookie("id_tour", $id.','.$_COOKIE['id_tour'], time()+3600, "/", "",  0);

            $cookie = $_COOKIE['id_tour'];
            $list_id = explode(",", $cookie);
            $seen_tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_picture','tour_blank','tour_status')->whereIn('id',$list_id)->where('tour_status',"=",1)->limit(6)->get();
        }else{
            $seen_tour = [];
            setcookie("id_tour", $id, time()+3600, "/", "",  0);
        }

        $tour_detail = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_description','tour_picture','tour_blank','tour_status','tour_hot')->where('id',"=",$id)->where('tour_status',"=",1)->first();
        $cat_parent_id = $tour_detail->tour_cat_id;
        $cate_child = DB::table('category')->select()->where('id',"=",$cat_parent_id)->get();
        $cate_child = Arr::flatten($cate_child);
        $cate_child = current($cate_child);
        $new_id = $cate_child->cat_parent_id;
        $cat_parent = DB::table('category')->select('id','cat_name','cat_slug','cat_type','cat_parent_id')->where('id','=',$new_id)->first();
        $cat_id = $tour_detail->tour_cat_id;
        $cate_tour = DB::table('category')->select('id','cat_name','cat_slug','cat_type','cat_parent_id')->where('id','=',$cat_id)->first();
        $cate_child_all = DB::table('category')->select()->where('cat_parent_id',"=",$new_id)->get()->toArray();
        $cate_child_all = Arr::random($cate_child_all, 8);
        $expediency = DB::table('expediency')->select('id','exp_name')->get();

        return view('web.pages.detail_tour.detail',compact('cate_child_all','tour_detail','expediency','cat_parent','cate_tour','seen_tour'));
    }

    public function getSearchTour(Request $request){
        $keyword = $request->keyword;
        $tour = DB::table('tour')->select()->where('tour_title','like',"%$keyword%")->get();

        return view('web.pages.search.search',compact('tour','keyword'));
    }

    public function getFilterTour($id){
        $place = DB::table('place')->select()->where('pla_type',"=",$id)->get();
        foreach($place as $pla){
            echo "<option value='".$pla->id."'>".$pla->pla_name."</option>";
        }
    }
    public function getFilterTourHome(Request $request){

        if(isset($_COOKIE['id_tour'])){
            $cookie = $_COOKIE['id_tour'];
            $list_id = explode(",", $cookie);
            $seen_tour = DB::table('tour')->select('id','tour_code','tour_slug','tour_type','tour_cat_id','tour_title','tour_pla_id','tour_price','tour_price_pro','tour_price_discount','tour_date_go','tour_date_back','tour_time','tour_from','tour_expediency','tour_picture','tour_blank','tour_status')->whereIn('id',$list_id)->where('tour_status',"=",1)->limit(6)->get();
        }else{
            $seen_tour = [];
        }

        $tour_type = $request->tourtype;
        $pla_id = $request->pla_id;
        $place_filter = DB::table('place')->select()->where('id','=',$pla_id)->get()->toArray();

        if(!empty($place_filter)){
            $place_filter = current($place_filter);
        }else{
            $place_filter = '';
        }



        if($pla_id == 0){
            $tour = DB::table('tour')->select()->where('tour_type','=',$tour_type)->get()->toArray();
            $item_tour = current($tour);
            $id_cat = $item_tour->tour_cat_id;
            $cat_right = DB::table('category')->select('id','cat_parent_id')->where('id','=',$id_cat)->get()->toArray();
            $cat_right = current($cat_right);
            $id_cat_parent = $cat_right->cat_parent_id;
            $all_cat_right = DB::table('category')->select('id','cat_parent_id','cat_slug','cat_name')->where('cat_parent_id','=',$id_cat_parent)->get()->toArray();
            $all_cat_right = Arr::random($all_cat_right, 8);
            // dd($all_cat_right);
        }else{
            $tour = DB::table('tour')->select()->where('tour_pla_id','=',$pla_id)->get()->toArray();
            if(!empty($tour)){
                $item_tour = current($tour);
                $id_cat = $item_tour->tour_cat_id;
                $cat_right = DB::table('category')->select('id','cat_parent_id')->where('id','=',$id_cat)->get()->toArray();
                $cat_right = current($cat_right);
                $id_cat_parent = $cat_right->cat_parent_id;
                $all_cat_right = DB::table('category')->select('id','cat_parent_id','cat_slug','cat_name')->where('cat_parent_id','=',$id_cat_parent)->get()->toArray();
                $all_cat_right = Arr::random($all_cat_right, 8);
            }else{
                $all_cat_right = '';
            }
        }

        return view('web.pages.filter_tour.filter',compact('tour','place_filter','all_cat_right','seen_tour'));
    }
}
