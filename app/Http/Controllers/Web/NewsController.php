<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //
    public function index($slug){
        $category = Category::Where('cat_slug',$slug)->first();
        $news = News::Where('new_active',1)->where('new_cat_id',$category->id)->orderBy('id','desc')->paginate(6);
        $news_latest_relate =  null;
        $news_new  = null;
        $is_check_classified = count($news->toArray()['data']);
        if ($is_check_classified== 0 ){
            $news = null;
        }
        else
        {
            $news_latest_relate = News::Where('new_active',1)->where('new_cat_id',$category->id)->orderBy('id','desc')->limit(3)->get();
            $news_new = $news->first();
        }

        return view('web.pages.news.list_news',compact('news','news_new','news_latest_relate','category'));
    }
    public function detail($slug,$id){
        $news  =  News::findOrFail($id);
        $category = Category::Where('id',$news->new_cat_id)->first();
        return view('web.pages.news.detail_news',compact('news','category'));
    }
}
