<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->subject = "Thông báo Inventore-trading Booking Ads";
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        dd(env('MAIL_PASSWORD'));
//        dd($this->subject);
        return $this->subject($this->subject)->replyTo('haivuvan@tech.admicro.vn', 'Vũ Văn Hải')->view('emails.send_mails', [
            'data' => $this->data
        ]);
    }

}
