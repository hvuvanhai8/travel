<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $table = 'banner';

    protected $fillable = [
        'ban_name',
        'ban_link',
        'ban_picture'
    ];
}
