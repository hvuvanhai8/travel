<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'booking';

    protected $fillable = [
        'book_cus_id',
        'book_status',
        'book_payment'
    ];
}
