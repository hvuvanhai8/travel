<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{
    //
    protected $table = 'booking_detail';

    protected $fillable = [
        'book_id',
        'book_detail_tour_id',
        'book_detail_quantity',
        'book_detail_status',
        'book_detail_payment'
    ];
}
