<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';

    protected $fillable = [
        'cat_name',
        'cat_rewrite',
        'cat_slug',
        'cat_type',
        'cat_active',
        'cat_parent_id',
        'cat_all_child',
    ];
}
