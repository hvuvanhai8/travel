<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customer';

    protected $fillable = [
        'cus_name',
        'cus_email',
        'cus_gender',
        'cus_age',
        'cus_old',
        'cus_passport',
        'cus_date_passport',
        'cus_nationlity',
        'cus_phone',
        'cus_address',
        'cus_picture',
        'cus_single_room',
        'cus_status',
        'cus_note'
    ];
}
