<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expediency extends Model
{
    //
    protected $table = 'expediency';

    protected $fillable = [
        'exp_name',
        'exp_status',
        'exp_value'
    ];
}
