<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    //
    protected $table = 'guide';

    protected $fillable = [
        'gui_code',
        'gui_name',
        'gui_gender',
        'gui_age',
        'gui_phone',
        'gui_address'
    ];
}
