<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'news';

    protected $fillable = [
        'new_cat_id',
        'new_user_id',
        'new_title',
        'new_teaser',
        'new_description',
        'new_picture'
    ];
}
