<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    //
    protected $table = 'options';

    protected $fillable = [
        'opt_position',
        'opt_key_position',
        'opt_value',
        'opt_description',
        'opt_active'
    ];
}
