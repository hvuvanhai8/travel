<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    //
    protected $table = 'place';

    protected $fillable = [
        'pla_code',
        'pla_name',
        'pla_description',
        'pla_picture',
        'pla_slide'
    ];
}
