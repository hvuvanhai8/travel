<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    //
    protected $table = 'promotion';

    protected $fillable = [
        'pro_tour_id',
        'pro_start_date',
        'pro_finish_date'
    ];
}
