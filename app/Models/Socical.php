<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socical extends Model
{
    //
    protected $table = 'socical';

    protected $fillable = [
        'soc_name',
        'soc_link',
        'soc_picture'
    ];
}
