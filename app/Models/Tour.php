<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    //
    protected $table = 'tour';

    protected $fillable = [
        'tour_code',
        'tour_cat_id',
        'tour_user_id',
        'tour_title',
        'tour_pla_id',
        'tour_price',
        'tour_price_foreign',
        'tour_price_child_foreign',
        'tour_guide_id',
        'tour_date_go',
        'tour_date_back',
        'tour_time',
        'tour_from',
        'tour_expediency',
        'tour_description',
        'tour_picture',
        'tour_slide',
        'tour_blank',
        'tour_status',
        'tour_note',
        'tour_hot',
    ];
}
