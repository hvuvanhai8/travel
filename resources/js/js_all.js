$('.single-item').owlCarousel({
    loop:true,
    margin:0,
    dots:false,
    nav:false,
    autoplay:true,
    autoplaySpeed: 2000,
    autoTimeout: 5000,
    animateOut: 'fadeOut',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


$('.slide_hot_tour').owlCarousel({
    loop:true,
    margin:12,
    dots:true,
    nav:true,
    autoplay:true,
    autoplaySpeed: 2000,
    autoTimeout: 4000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:5
        }
    }
});

// tỉnh tổng số lượng trong book

