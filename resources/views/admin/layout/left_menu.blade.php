<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="active treeview menu-open">
        <a href="#">
        <i class="fa fa-dashboard"></i> <span>Thống kê</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-circle-o"></i> Bảng thống kê</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-fw fa-laptop"></i>
        <span>Quản lý đơn hàng</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{route('book.index')}}"><i class="fa fa-fw fa-list-ul"></i> Đơn hàng mới</a></li>
            <li><a href="{{route('book.success')}}"><i class="fa fa-fw fa-list-ul"></i> Đơn hàng đã thanh toán</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý banner</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin/banner"><i class="fa fa-fw fa-list-ul"></i>Danh sách banner</a></li>
            <li><a href="/admin/banner/create"><i class="fa fa-fw fa-plus-square-o"></i>Thêm danh banner</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-fw fa-laptop"></i>
        <span>Quản lý danh mục</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{route('category.index')}}"><i class="fa fa-fw fa-list-ul"></i> Danh sách danh mục</a></li>
        <li><a href="{{route('category.create')}}"><i class="fa fa-fw fa-plus-square-o"></i> Thêm danh mục</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-fw fa-plane"></i>
        <span>Quản lý tour du lịch</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{route('tour.index')}}"><i class="fa fa-fw fa-list-ul"></i>Danh sách tour</a></li>
        <li><a href="{{route('tour.create')}}"><i class="fa fa-fw fa-plus-square-o"></i>Thêm tour mới</a></li>
        </ul>
    </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-fw fa-newspaper-o"></i>
            <span>Quản lý tin tức</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="/admin/news"><i class="fa fa-fw fa-list-ul"></i>Danh sách tin tức</a></li>
            <li><a href="/admin/news/create"><i class="fa fa-fw fa-plus-square-o"></i>Thêm tin tức mới</a></li>
            </ul>
        </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý địa điểm du lịch</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin/place"><i class="fa fa-fw fa-list-ul"></i>Danh sách địa điểm</a></li>
            <li><a href="/admin/place/create"><i class="fa fa-fw fa-plus-square-o"></i>Thêm danh địa điểm</a></li>
        </ul>
    </li>
</ul>