@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div><ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Banner</li>
            </ol></div>
        <h1>
            Banner
            <small>Add</small>
        </h1>
    </section>
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Add</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" enctype="multipart/form-data" action="{{asset('admin/banner')}}" method="POST">
                        @csrf
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Banner Name</label>
                                    <input type="text" class="form-control" name="ban_name" id="" placeholder="Enter banner name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Trạng thái</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="ban_active" checked> Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleInputFile">Photo</label>
                                <input type="file" name="ban_picture" id="" multiple="multiple">
                            </div>
                        </div>
                        <div class="box-footer" style="margin-left: 20px">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection