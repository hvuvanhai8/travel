@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li>Quản lý banner du lịch</li>
                <li class="active">Danh sách các banner</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-2">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách banner</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên banner</th>
                                <th>Người đăng tin</th>
                                <th>Ngày đăng</th>
                                <th>Trạng thái</th>
                                <th>Action</th>
                            </tr>
                            <tr>
                                @foreach($banner as $key=>$item_ban)
                                    <td>{{$key+1}}</td>
                                    @if(isset($item_ban->ban_picture))
                                        <td>
                                            <div class="img_pla" style="width: 100px;">
                                                <img src="{{asset('uploadfile/banner/').'/'.$item_ban->ban_picture}}" width="100%" height="100%" alt="">
                                            </div>
                                        </td>
                                    @else
                                        <td><img src="" alt=""></td>
                                    @endif
                                    <td style="width:300px">{{$item_ban->ban_name}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>
                                        <div class="create_at">{{ $item_ban->created_at }}</div>
                                    </td>
                                    @if($item_ban->ban_active===1)
                                        <td><span class="label label-success">Active</span></td>
                                    @else
                                        <td><span class="label label-danger">No Active</span></td>
                                    @endif
                                    <td>
                                        <a class="btn btn-success" data-toggle="modal" data-target="#modal-info-17" title="chi tiết"><i class="fa fa-fw fa-info"></i></a>
                                        <a class="btn btn-warning" href="{{route('banner.edit',[$item_ban->id])}}"><i class="fa fa-edit"></i> Sửa</a>
                                        <a class="btn btn-danger" href="{{route('banner.destroy',[$item_ban->id])}}"><i class="fa fa-trash"></i>Xoá</a>
                                    </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{route('banner.create')}}">Thêm</a>
                        <div class="paginate_cate text-center">
                            {{$banner->links()}}
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection