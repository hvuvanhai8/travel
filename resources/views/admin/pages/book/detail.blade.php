@php
    // dd($user_book  )   
@endphp

@extends('admin.layout.master')

@section('content') 
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li><a href="{{route('book.index')}}" title="danh sách đơn hàng">Danh sách đơn hàng</a> </li>
                <li class="active">Chi tiết đơn hàng</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Thay đổi trạng thái đơn hàng #{{$book->id}}</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div class="col-md-2">
                            <select class="form-control" name="" id="book_payment">
                                <option value="1" {{($book->book_payment == 1 ? 'selected':'')}}>Đã thanh toán</option>
                                <option value="0" {{($book->book_payment == 0 ? 'selected':'')}}>Chưa thanh toán</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="paginate_cate text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">Chi tiết đơn hàng #{{$book->id}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding bookdetail">
                    <div class="div_order">
                        <div class="text-center">
                            <div class="tit_order">
                                THÔNG TIN NGƯỜI ĐẶT
                            </div>
                        </div>
                        <table class="table table-bordered" id="tbl_tourdetail">
                            <tbody>
                                <tr>
                                    <td>
                                        Họ tên
                                    </td>
                                    <td>
                                        {{$user_book->cus_name}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Điện thoại
                                    </td>
                                    <td> {{$user_book->cus_phone}}</td>
                                </tr>
                                <tr>
                                    <td> Địa chỉ</td>
                                    <td> {{$user_book->cus_address}}</td>
                                </tr>
                                <tr>
                                    <td>Email  </td>
                                    <td>{{$user_book->cus_email}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="div_order">
                        <div class="">
                            <div class="tit_tt_khachhang_coban">
                                THÔNG TIN ĐƠN HÀNG
                            </div>
        
                        </div>
                        <table class="table table-bordered" id="tbl_tourdetail">
                            <tbody>
                                <tr>
                                    <td>
                                        Mã đơn hàng
                                    </td>
                                    <td>
                                        #{{$book->id}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tour đặt
                                    </td>
                                    <td>
                                    <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}">{{$tour_detail->tour_title}}</a>
                                   </td>
                                </tr>
                                <tr>
                                    <td>
                                        Số lượng
                                    </td>
                                    <td>
                                        {{$book_detail->book_detail_quantity}}
                                   </td>
                                </tr>
                                <tr>
                                    <td>
                                        Trị giá booking
                                    </td>
                                    <td>
                                        Liên hệ
                                   </td>
                                </tr>
                                <tr>
                                    <td> Ngày gửi</td>
                                    <td>{{$book->updated_at}}</td>
                                </tr>
                                <tr>
                                    <td>Hinh thức thanh toán  </td>
                                    <td>
                                        Thanh toán sau (Chuyển khoản/tiền mặt)
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tình trạng thanh toán</td>
                                    <td>
                                        @if($book->book_payment == 0)
                                            <span> Chưa thanh toán</span>
                                        @else
                                            <span> Đã thanh toán</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="div_order">
                        <div class="tit_tt_khachhang_coban">
                            DANH SÁCH KHÁCH ĐI TOUR
                        </div>
                        <table class="table table-bordered tableRefix" id="tbl_tourdetail">
                            <thead>
                                <tr class="bgcolor">
                                    <th class="auto-style1">
                                        Họ và tên
                                    </th>
                                    <th class="auto-style1">Quốc tịch</th>
                                    <th class="auto-style1">Độ tuổi</th>
                                    <th class="auto-style1">Giới tính</th>
                                    <th class="auto-style1">CMND/Hộ chiếu</th>
                                    <th class="auto-style1">Ngày hết hạn</th>
                                    <th class="auto-style1">Phòng đơn</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list_cus as $cus)
                                <tr>
                                    <td data-title="Họ tên:">{{$cus->cus_name}}</td>
                                    <td data-title="Quốc tịch:"><span>{{($cus->cus_nationlity == 1 ? 'Việt Nam':'Nước ngoài')}}</span></td>
                                    <td data-title="Độ tuổi:"><span>{{$cus->cus_old}}</span></td>
                                    <td data-title="Giới tính:"><span>{{$cus->cus_gender}}</span></td>
                                    <td data-title="CMND/Hộ chiếu:"><span>{{$cus->cus_passport}}</span></td>
                                    <td data-title="Ngày hết hạn:"><span>{{$cus->cus_date_passport}}</span></td>
                                    <td data-title="Phòng đơn:"><span>{{($cus->cus_single_room == 1 ? 'Có':'Không')}}</span></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="paginate_cate text-center">
                            
                    </div>
                </div>
            </div>
            <!-- /.box -->
            </div>
        </div>
        
    </section>
@endsection

@section('script')

<script>
 
    $(document).ready(function(){
        $('#book_payment').change(function(){
            var status = $(this).val();
            var id = "@php echo $book->id; @endphp";
            $.get("/admin/booking/success/change/"+id+"/"+status,function(data){
                alert('Đã thay đổi trạng thái đơn hàng !');
                window.location.href = "/admin/booking/success";
            });
        });
    });
</script>

@endsection