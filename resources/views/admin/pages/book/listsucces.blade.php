@php
//  dd(123);
@endphp

@extends('admin.layout.master')

@section('content') 
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li>Quản lý đơn hàng</li>
                <li class="active">Danh sách đơn hàng thành công</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">Danh sách đơn hàng thành công ( Đã thanh toán )</h3>
    
                <div class="box-tools">
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Tìm kiếm đơn">
    
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                    </div>
                </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                    <th style="width: 40px;">STT</th>
                    <th style="width: 70px;">Mã đơn</th>
                    <th style="width: 250px">Người đặt đơn</th>
                    <th>Giá đơn hàng</th>
                    <th >Thời gian đặt hàng</th>
                    <th >Trạng thái thanh toán</th>
                    <th>Hành động</th>
                    </tr>
                    @php $stt = 1; @endphp
                    @foreach ($book as $bo)
                    <tr>
                    <td>{{$stt++}}</td>
                    <td>#{{$bo->id}}</td>
                    @foreach($user_book as $user)  
                    @if($bo->book_cus_id == $user->id) 
                    <td><i class="fa fa-fw fa-user"></i> {{$user->cus_name}}</td>
                    @endif
                    @endforeach
                    <td><i class="fa fa-fw fa-money"></i> {{number_format($bo->book_total)}}</td>
                    <td><i class="fa fa-fw fa-calendar-check-o"></i> {{$bo->updated_at}}</td>
                    <td>
                        @if($bo->book_payment == 0)
                        <span class="label label-danger">Chưa thanh toán</span>
                        @else
                        <span class="label label-success">Đã thanh toán</span>
                        @endif
                    </td>
                    <td> <a class="btn btn-success" href="{{asset('admin/booking')}}/{{$bo->id}}" title="chi tiết"><i class="fa fa-fw fa-info"></i> Chi tiêt</a>
                    </tr>
                @endforeach
                </tbody></table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="paginate_cate text-center">
                        {{$book->links()}}
                    </div>
                </div>
            </div>
            <!-- /.box -->
            </div>
        </div>
    </section>
@endsection