@php
    
// dd($errors->all());
@endphp

@extends('admin.layout.master')

@section('content') 
<section class="content-header">
    <div><ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{asset('admin/child/category')}}/{{$cate[0]->id}}">Danh sách danh mục con</a></li>
            <li class="active">Thêm danh mục con</li>
        </ol></div>
    <h1>
    Thêm danh mục con mới cho danh mục "{{$cate[0]->cat_name}}"
    </h1>
</section>
<section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-4">
          @if(count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
            <form action="{{asset('admin/child/category/add')}}/{{$cate[0]->id}}" role="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Tên Danh mục con</label>
                            <input type="text" name="cat_name" class="form-control" placeholder="Nhập tên danh mục con" required>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Thuộc địa điểm du lịch *</label>
                            <select class="form-control" name="cat_place_id">
                              <option value="0">Chọn địa điểm</option>
                              @foreach($place as $pla)
                              <option value="{{$pla->id}}">{{$pla->pla_name}}</option>
                              @endforeach  
                            </select>
                        </div>
                      </div>
                  </div>
                </div>
                <!-- /.box-body -->
  
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
            </div>
          </div>
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')


</script>

@endsection

@section('style')

<style>
    
</style>
    
@endsection