@php
// dd($cate_child);

@endphp

@extends('admin.layout.master')

@section('content') 
<section class="content-header">
    <div><ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{route('category.index')}}">Danh sách danh mục</a></li>
            <li><a href="{{asset('admin/child/category')}}/{{$cate_child->cat_parent_id}}">Danh sách danh mục con</a></li>
            <li class="active">Sửa danh mục con</li>
        </ol></div>
    <h1>
    Sửa tên danh mục con
    </h1>
</section>
<section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-4">
          @if(count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
            <form action="{{asset('admin/child/category/edit')}}/{{$cate_child->id}}" role="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Tên Danh mục con</label>
                            <input type="text" name="cat_name" value="{{$cate_child->cat_name}}" class="form-control" placeholder="Nhập tên danh mục con" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Thuộc địa điểm du lịch * ( chỉ dành cho danh mục tour )</label>
                            <select class="form-control" name="cat_place_id" {{$cate_child->cat_type == 'tintuc' ? 'disabled':''}}>
                              <option value="0">Chọn địa điểm du lịch</option>
                              @foreach($place as $pla)
                              <option value="{{$pla->id}}" {{$cate_child->cat_place_id == $pla->id ? "selected":''}}>{{$pla->pla_name}}</option>
                              @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                          <label>Active</label>
                          <div class="checkbox">
                              <label>
                                  <input type="checkbox" name="cat_active" value="1">
                              </label>
                          </div>
                      </div>
                  </div>
                </div>
                <!-- /.box-body -->
  
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
              </form>
            </div>
          </div>
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')


</script>

@endsection

@section('style')

<style>
    
</style>
    
@endsection