@php 
// dd($cate_child);
@endphp

@extends('admin.layout.master')

@section('content') 
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{route('category.index')}}">Danh sách danh mục</a></li>
            <li class="active">Danh sách danh mục con</li>
            </ol>
        </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
            @if (session('success'))
              <div class="alert alert-success">
                    <strong>{{ session('success') }}</strong>
              </div>
            @endif
        </div>
      </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                      @if(!empty(Arr::flatten($cate_child)))
                      <h3 class="box-title">Danh sách danh mục con <strong>"{{$cate[0]->cat_name}}"</strong></h3>
                      @else
                      <h3 class="box-title">Không có danh mục con nào !</h3>
                      @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table class="table table-bordered">
                        <tbody><tr>
                          <th style="width: 10px">STT</th>
                          <th>Danh mục con</th>
                          <th>Loại danh mục</th>
                          <th style="width: 300px">Hành động</th>
                        </tr>
                        @php $stt = 1; @endphp
                        @foreach ($cate_child as $cat)
                        <tr>
                        <td>{{$stt++}}</td>
                        <td style="text-transform: uppercase;">{{$cat->cat_name}}</td>
                        <td>{{$cat->cat_type}}</td>
                        <td>
                          <a class="btn btn-warning" href="{{asset('admin/child/category/edit')}}/{{$cat->id}}"><i class="fa fa-edit"></i> Sửa</a>
                          <a class="btn btn-danger" data-toggle="modal" data-target="#modal-default-{{$cat->id}}"><i class="fa fa-trash"></i> Xoá</a>
                        </td>
                        </tr>

                        <div class="modal fade" id="modal-default-{{$cat->id}}" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                  <h4 class="modal-title">Bạn có chắn chắn muốn xóa {{$cat->cat_name}} ?</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy bỏ</button>
                                    <a href="{{asset('admin/child/category/delete/')}}/{{$cat->id}}" class="btn btn-danger">Xóa bỏ</a>
                                </div>
                              </div>
                            </div>
                        </div>
                        @endforeach
                      </tbody></table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                    <a class="btn btn-primary" href="{{asset('admin/child/category/add/')}}/{{$cate[0]->id}}">Thêm mới</a>
                    <div class="paginate_cate text-center">
                        {{$cate_child->links()}}
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
@endsection