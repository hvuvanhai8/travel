@php
    
// dd($errors->all());
@endphp

@extends('admin.layout.master')

@section('content') 
<section class="content-header">
    <div><ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{route('category.index')}}">Danh sách danh mục</a></li>
            <li class="active">Thêm danh mục</li>
        </ol></div>
    <h1>
        Thêm danh mục mới
    </h1>
</section>
<section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-4">
          @if(count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
            <form action="{{route('category.store')}}" role="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Tên Danh mục</label>
                            <input type="text" name="cat_name" class="form-control" placeholder="Nhập tên danh mục">
                        </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label>Loại Danh mục</label>
                              <div>
                                  <select name="cat_type" id="" style="width: 100%; height: 34px">
                                      <option value="0" selected> - Chọn loại danh mục</option>
                                      <option value="trongnuoc">Du lịch trong nước</option>
                                      <option value="doanhnghiep">Du lịch cho doanh nghiệp</option>
                                      <option value="nuocngoai">Du lịch nước ngoài</option>
                                      <option value="tintuc">Tin tức du lịch</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
                <!-- /.box-body -->
  
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
            </div>
          </div>
    </div>
    <!-- /.row -->
</section>
@endsection