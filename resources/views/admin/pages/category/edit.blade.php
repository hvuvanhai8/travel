@php
    
@endphp

@extends('admin.layout.master')

@section('content') 
<section class="content-header">
    <div><ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{route('category.index')}}">Danh sách danh mục</a></li>
            <li class="active">Sửa danh mục</li>
        </ol></div>
    <h1>
        Sửa danh mục
    </h1>
</section>
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-4">
            @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
              <!-- /.box-header -->
              <!-- form start -->
            <form action="{{asset('admin/category')}}/{{$cat->id}}/edit" role="form" method="GET" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label>Nhập tên danh mục mới *</label>
                        <input type="text" name="cat_name" value="{{$cat->cat_name}}" class="form-control" placeholder="Nhập tên danh mục">
                        </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label>Loại Danh mục</label>
                              <div>
                                  <select name="cat_type" id="" style="width: 100%; height: 34px">
                                        <option value="0" selected> - Chọn loại danh mục</option>
                                        <option value="trongnuoc">Du lịch trong nước</option>
                                        <option value="doanhnghiep">Du lịch cho doanh nghiệp</option>
                                        <option value="nuocngoai">Du lịch nước ngoài</option>
                                        <option value="tintuc">Tin tức du lịch</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
                <!-- /.box-body -->
  
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
              </form>
            </div>
          </div>
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')




@endsection

@section('style')

<style>
    
</style>
    
@endsection