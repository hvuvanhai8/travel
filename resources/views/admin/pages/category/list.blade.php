@php 

@endphp

@extends('admin.layout.master')

@section('content') 
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Danh sách danh mục</li>
            </ol>
        </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
            @if (session('success'))
              <div class="alert alert-success">
                    <strong>{{ session('success') }}</strong>
              </div>
            @elseif(session('error'))
            <div class="alert alert-danger">
              <strong>{{ session('error') }}</strong>
            </div>
            @endif
        </div>
      </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Danh sách danh mục</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table class="table table-bordered">
                        <tbody><tr>
                          <th style="width: 10px">STT</th>
                          <th>Danh mục</th>
                            <th>Loại danh mục</th>
                          <th style="width: 300px">Hành động</th>
                        </tr>
                        @php $stt = 1; @endphp
                        @foreach ($category as $cat)
                        <tr>
                        <td>{{$stt++}}</td>
                        <td style="text-transform: uppercase;">{{$cat->cat_name}}</td>
                            <td style="width: 100px">{{$cat->cat_type}}</td>
                          <td>
                            <a class="btn btn-success" href="{{asset('admin/child/category/')}}/{{$cat->id}}"><i class="fa fa-fw fa-list-ul"></i> Danh mục con</a>
                            <a class="btn btn-warning" href="{{asset('admin/category')}}/{{$cat->id}}"><i class="fa fa-edit"></i> Sửa</a>
                            <a class="btn btn-danger hidden"  data-toggle="modal" data-target="#modal-default-{{$cat->id}}"><i class="fa fa-trash"></i> Xoá</a>
                          </td>
                        </tr>

                        <div class="modal fade" id="modal-default-{{$cat->id}}" style="display: none;">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Bạn có chắn chắn muốn xóa {{$cat->cat_name}} ?</h4>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy bỏ</button>
                                <a href="{{asset('admin/delete')}}/{{$cat->id}}" class="btn btn-danger">Xóa bỏ</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                      </tbody></table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                    <a class="btn btn-primary" href="{{route('category.create')}}">Thêm</a>
                      <div class="paginate_cate text-center">
                        {{$category->links()}}
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
@endsection