@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div><ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">News</li>
            </ol></div>
        <h1>
            News
            <small>Edit</small>
        </h1>
    </section>
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Edit</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" enctype="multipart/form-data" action="{{asset('admin/news').'/'.$news->id}}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">News Title</label>
                                    <input type="text" class="form-control" name="new_title" id="" value="{{$news->new_title}}" placeholder="Enter news title">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Danh sách tin tức</label>
                                    <select name="new_cat_id" id="" style="width: 100%; height: 34px">
                                        @foreach($cat_news_child as $cat)
                                            @if($cat->id==$news->new_cat_id)
                                                <option value="{{$cat->id}}" selected>{{$cat->cat_name}}</option>
                                            @else
                                                <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding: 10px 30px;">
                                    <label>Mô tả tổng quan *</label>
                                    <textarea name="new_description">{!! $news->new_description !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleInputFile">Photo</label>
                                <input type="file" name="new_picture" id="" multiple="multiple">

                                <div class="img_old">
                                    <img src="{{asset('uploadfile/news/').'/'.$news->new_picture}}" width="200px" height="200px" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="margin-left: 20px">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')

    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>


    <script>
        var editor = CKEDITOR.replace( 'new_description',{
            disallowedContent:'a[!href];p(tip)'
        });

        editor.on( 'instanceReady', function() {
            console.log( editor.filter.allowedContent );
        } );

    </script>

@endsection