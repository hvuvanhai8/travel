@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li>Quản lý tin tức du lịch</li>
                <li class="active">Danh sách các tin tức</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-2">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách tất cả các tin</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tiêu đề</th>
                                <th>Người đăng tin</th>
                                <th>Danh mục tin tức</th>
                                <th>Ngày đăng</th>
                                <th>Trạng thái</th>
                                <th>Action</th>
                            </tr>
                            <tr>
                                @foreach($news as $key=>$item_news)
                                    <td>{{$key+1}}</td>
                                    @if(isset($item_news->new_picture))
                                        <td>
                                            <div class="img_pla" style="width: 100px;">
                                                <img src="{{asset('uploadfile/news/').'/'.$item_news->new_picture}}" width="100%" height="100%" alt="">
                                            </div>
                                        </td>
                                    @else
                                        <td><div class="img_pla" style="width: 100px;"><img src="{{asset('web/images/default.jpg')}}" width="100%" height="100%" alt=""></div></td>
                                    @endif
                                    <td style="width:300px">{{$item_news->new_title}}</td>
                                    <td>{{$user->name}}</td>
                                    @foreach($categories as $cat)
                                        @if($cat->id === $item_news->new_cat_id)
                                            <td>{{$cat->cat_name}}</td>
                                        @endif
                                    @endforeach
                                    <td>
                                        <div class="create_at">{{ $item_news->created_at }}</div>
                                    </td>
                                    @if($item_news->new_active===1)
                                        <td><span class="label label-success">Active</span></td>
                                    @else
                                        <td><span class="label label-danger">No Active</span></td>
                                    @endif
                                    <td>
                                        <a class="btn btn-success" data-toggle="modal" data-target="#modal-info-{{$item_news->id}}" title="chi tiết"><i class="fa fa-fw fa-info"></i></a>
                                        <a class="btn btn-warning" href="{{route('news.edit',[$item_news->id])}}"><i class="fa fa-edit"></i> Sửa</a>
                                        <a class="btn btn-danger" href="{{route('news.destroy',[$item_news->id])}}"><i class="fa fa-trash"></i>Xoá</a>
                                    </td>
                                </tr>
                                <div class="modal fade in" id="modal-info-{{$item_news->id}}" style="display: none;">
                                    <div class="modal-dialog modal_info">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" style="font-weight: bold">{{$item_news->new_title}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="content_info">
                                                    <div class="header_body header_news">
                                                        <div class="img_news">
                                                            <img src="{{asset('uploadfile/news'.'/'.$item_news->new_picture)}}" alt="Ảnh" width="250" height="230">
                                                        </div>
                                                    </div>
                                                    <div class="content_news">
                                                        <div class="info_news">
                                                            <p class="price">Người đăng : <b>{{$user->name}}</b></p>
                                                            <p>Ngày đăng tin : <b>{{ $item_news->created_at }}</b></p>
                                                            @foreach($categories as $cat)
                                                                @if($cat->id === $item_news->new_cat_id)
                                                                    <p class="price">Danh mục tin tức : <b>{{$cat->cat_name}}</b></p>
                                                                @endif
                                                            @endforeach
                                                            @if($item_news->new_active===1)
                                                                <p>Trạng thái : <b><span class="label label-success">Active</span></b></p>
                                                            @else
                                                                <p>Trạng thái : <b><span class="label label-danger">No Active</span></b></p>
                                                            @endif
                                                        </div>
                                                        <div class="description_news">
                                                            {!! $item_news->new_description !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{route('news.create')}}">Thêm</a>
                        <div class="paginate_cate text-center">
                            {{$news->links()}}
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
@section('style')

    <style>
        .content_info{
            display: grid;
            grid-template-columns: 200px 1fr;
            grid-gap: 10px;
        }
        .header_news{
            width: 100%;
            display: inline-block !important;
        }
        .info_news{
            display: grid;
            grid-template-columns: 1fr 1fr;
            padding-bottom: 20px;
            border-bottom: 1px solid #777777c7;
        }
        .img_news{
            margin-bottom: 20px;
            border:1px solid #777777c7;
        }
        .img_news img{
            width: 100%;
            height: 200px;
        }
        .description_news{
            margin-top: 20px;
        }
    </style>

@endsection