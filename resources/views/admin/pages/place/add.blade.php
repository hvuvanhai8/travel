@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div><ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li><a href="{{route('place.index')}}">Danh sách địa điểm</a></li>
                <li class="active">Thêm địa điểm du lịch</li>
            </ol></div>
        <h1>
            Thêm địa điểm du lịch
        </h1>
    </section>
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" enctype="multipart/form-data" action="{{asset('admin/place')}}" method="POST">
                        @csrf
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" class="form-control" name="pla_name" id="" placeholder="Enter place name">
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="pla_type" id="optionsRadios1" value="1" checked="">
                                            Trong nước
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="pla_type" id="optionsRadios2" value="0">
                                            Nước ngoài
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zip Code</label>
                                    <input type="text" class="form-control" name="pla_code" id="" placeholder="Enter zip code">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding: 10px 30px;">
                                    <label>Mô tả tổng quan *</label>
                                    <textarea name="pla_description"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleInputFile">Photo</label>
                                <input type="file" name="pla_photo" id="" multiple="multiple">
                            </div>
                        </div>
                        <div class="box-footer" style="margin-left: 20px">
                            <button type="submit" class="btn btn-primary">Thêm</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')

    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>


    <script>
        CKEDITOR.replace( 'pla_description' );
    </script>

@endsection