@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Danh sách địa điểm</li>
            </ol>
        </div>
        <h1>
            Danh sách địa điểm du lịch
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="mt-3">
                    <ul class="nav nav-tabs">
                      <li class="nav-item active">
                        <a class="nav-link active" data-toggle="tab" href="#trongnuoc">Trong nước</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nuocngoai">Nước ngoài</a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div id="trongnuoc" class="tab-pane active">
                        <div class="box box-primary">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <th>STT</th>
                                            <th>Picture</th>
                                            <th>Place Name</th>
                                            <th>Ngày tạo</th>
                                            <th>Mô tả</th>
                                            <th>Địa điểm</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                        @foreach($places_tn as $key=>$pla)
                                            <tr>
                                                <td style="text-align: center">{{$key+1}}</td>
                                                @if(isset($pla->pla_picture))
                                                    <td>
                                                        <div class="img_pla" style="width: 100px;">
                                                            <img src="{{asset('uploadfile/places/').'/'.$pla->pla_picture}}" width="100%" height="100%" alt="">
                                                        </div>
                                                    </td>
                                                @else
                                                    <td><img src="" alt=""></td>
                                                @endif
                                                <td>{{$pla->pla_name}}</td>
                                                <td>{{$pla->created_at}}</td>
                                                <td><div class="content_place">{!! $pla->pla_description !!}</div></td>
                                                <td>{{$pla->pla_type==1?'trong nước':'nước ngoài'}}</td>
                                                @if($pla->pla_active===1)
                                                    <td><span class="label label-success">Hiển thị</span></td>
                                                @else
                                                    <td><span class="label label-danger">Đã ẩn</span></td>
                                                @endif
                                                <td>
                                                    <a class="btn btn-warning" href="{{'/admin/place/'.$pla->id.'/edit'}}"><i class="fa fa-edit"></i> Sửa</a>
                                                    <a class="btn btn-danger hidden" href="#"><i class="fa fa-trash"></i> Xoá</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a class="btn btn-primary" href="/admin/place/create">Thêm</a>
                            </div>
                        </div>
                      </div>
                      <div id="nuocngoai" class="tab-pane fade">
                        <div class="box box-primary">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <th>STT</th>
                                            <th>Picture</th>
                                            <th>Place Name</th>
                                            <th>Ngày tạo</th>
                                            <th>Mô tả</th>
                                            <th>Địa điểm</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                        @foreach($places_nn as $key=>$pla)
                                            <tr>
                                                <td style="text-align: center">{{$key+1}}</td>
                                                @if(isset($pla->pla_picture))
                                                    <td>
                                                        <div class="img_pla" style="width: 100px;">
                                                            <img src="{{asset('uploadfile/places/').'/'.$pla->pla_picture}}" width="100%" height="100%" alt="">
                                                        </div>
                                                    </td>
                                                @else
                                                    <td><img src="" alt=""></td>
                                                @endif
                                                <td>{{$pla->pla_name}}</td>
                                                <td>{{$pla->created_at}}</td>
                                                <td><div class="content_place">{!! $pla->pla_description !!}</div></td>
                                                <td>{{$pla->pla_type==1?'trong nước':'nước ngoài'}}</td>
                                                @if($pla->pla_active===1)
                                                    <td><span class="label label-success">Hiển thị</span></td>
                                                @else
                                                    <td><span class="label label-danger">Đã ẩn</span></td>
                                                @endif
                                                <td>
                                                    <a class="btn btn-warning" href="{{'/admin/place/'.$pla->id.'/edit'}}"><i class="fa fa-edit"></i> Sửa</a>
                                                    <a class="btn btn-danger hidden" href="#"><i class="fa fa-trash"></i> Xoá</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a class="btn btn-primary" href="/admin/place/create">Thêm</a>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
            </div>
        </div>
    </section>
@endsection