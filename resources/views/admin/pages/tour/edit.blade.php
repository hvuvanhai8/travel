@php
//  dd($user);   
@endphp

@extends('admin.layout.master')

@section('content') 
<section class="content-header">
    <div><ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="{{route('tour.index')}}">Danh sách tour</a></li>
            <li class="active">Sửa tour</li>
        </ol></div>
    <h1>
        Sửa tour
    </h1>
</section>
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-4">
            @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <form role="form" action="{{asset('admin/tour/update/')}}/{{$tour->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                    <label>Tiêu đề tour</label>
                    <input type="text" name="tour_title" value="{{$tour->tour_title}}" class="form-control" placeholder="tiêu đề" required>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mã tour</label>
                            <input type="text" name="tour_code" value="{{$tour->tour_code}}" class="form-control" placeholder="code" >
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Người đăng tour</label>
                                <select class="form-control" name="tour_user_id">
                                    @foreach($user as $use)
                                    <option value="{{$use->id}}" {{$tour->tour_user_id == $use->id ? "selected":''}}>{{$use->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Địa điểm chính</label>
                                <select class="form-control" name="tour_pla_id">
                                    @foreach($place as $pla)
                                    <option value="{{$pla->id}}" {{$tour->tour_pla_id == $pla->id ? "selected":''}}>{{$pla->pla_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Danh mục tour</label>
                                <select class="form-control" name="tour_cat_id">
                                    @foreach($category as $cat)
                                    <option value="{{$cat->id}}" {{$tour->tour_cat_id == $cat->id ? "selected":''}}>{{$cat->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Loại tour</label>
                        <div class="radio">
                            <label>
                            <input type="radio" name="tour_type" value="1" {{($tour->tour_type == 1 ? 'checked':'')}}>
                            Trong nước
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            <input type="radio" name="tour_type" value="0" {{($tour->tour_type != 1 ? 'checked':'')}}>
                            Nước ngoài
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Giá tour</label>
                                <input type="text" value="{{$tour->tour_price}}" name="tour_price" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="5,000,000 đ">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Giá tour khuyến mãi</label>
                                <input type="text" value="{{$tour->tour_price_pro}}" name="tour_price_pro" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="3,990,000 đ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá tour trẻ em ( việt nam )</label>
                                <input type="text" value="{{$tour->tour_price_child}}" name="tour_price_child" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="3,000,000 đ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá tour em bé ( việt nam )</label>
                                <input type="text" value="{{$tour->tour_price_baby}}" name="tour_price_baby" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="800,000 đ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá tour ( Người nước ngoài )</label>
                                <input type="text" value="{{$tour->tour_price_foreign}}" name="tour_price_foreign" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="6,500,000 đ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá tour trẻ em ( nước ngoài )</label>
                                <input type="text" value="{{$tour->tour_price_child_foreign}}" name="tour_price_child_foreign" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="4,500,000 đ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá tour em bé ( nước ngoài )</label>
                                <input type="text" value="{{$tour->tour_price_baby_foreign}}" name="tour_price_baby_foreign" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="1,000,000 đ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Giá phụ thu phòng đơn</label>
                                <input type="text" value="{{$tour->tour_price_single_room}}" name="tour_price_single_room" class="form-control" onkeyup="this.value=FormatNumber(this.value);" placeholder="500,000 đ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Số chỗ trống</label>
                                <input type="text" value="{{$tour->tour_blank}}" name="tour_blank" class="form-control" onkeyup="this.value=IsNumberInt(this.value);" placeholder="50">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hướng dẫn viên</label>
                                <select class="form-control">
                                    <option value="0">Không có</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Phương tiện</label>
                                <select class="form-control" name="tour_expediency">
                                    @foreach ($expediency  as $exp)
                                    <option value="{{$exp->id}}" {{$tour->tour_expediency == $exp->id ? "selected":''}}>{{$exp->exp_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Địa điểm khởi hành</label>
                                <input type="text" value="{{$tour->tour_from}}" name="tour_from" class="form-control" placeholder="Hà nội">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Ngày đi *:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" value="{{$tour->tour_date_go}}" name="tour_date_go" class="form-control pull-right">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Ngày về *:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" value="{{$tour->tour_date_back}}" name="tour_date_back" class="form-control pull-right">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mô tả thời gian</label>
                                <input type="text" value="{{$tour->tour_time}}"  name="tour_time" class="form-control" placeholder="4 ngày 3 đêm">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Mô tả tổng quan *</label>
                            <textarea name="tour_description">{!!$tour->tour_description!!}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Ảnh chính tour</label>
                        <input type="file" name="tour_picture">
        
                        <p class="help-block">Bạn nên chọn ảnh đẹp.</p>
                        </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="tour_hot" {{($tour->tour_hot == 1 ? 'checked':'')}}> Tour hot ( Tour hiển thị ở slide trang chủ ) 
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="">Trạng thái</label>
                        <div class="radio">
                            <label>
                            <input type="radio" name="tour_status" value="1"  {{($tour->tour_status == 1 ? 'checked':'')}}>
                            Hiển thị
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            <input type="radio" name="tour_status" value="0" {{($tour->tour_status == 0 ? 'checked':'')}}>
                            Ẩn tour
                            </label>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
    
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
@endsection

@section('script')

<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>


<script>
    CKEDITOR.replace( 'tour_description' );

    var inputnumber = 'Giá trị nhập vào không phải là số';
	function FormatNumber(str) {
		var strTemp = GetNumber(str);
		if (strTemp.length <= 3)
			return strTemp;
		strResult = "";
		for (var i = 0; i < strTemp.length; i++)
			strTemp = strTemp.replace(",", "");
		var m = strTemp.lastIndexOf(".");
		if (m == -1) {
			for (var i = strTemp.length; i >= 0; i--) {
				if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
					strResult = "," + strResult;
				strResult = strTemp.substring(i, i + 1) + strResult;
			}
		} else {
			var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
			var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
					strTemp.length);
			var tam = 0;
			for (var i = strphannguyen.length; i >= 0; i--) {

				if (strResult.length > 0 && tam == 4) {
					strResult = "," + strResult;
					tam = 1;
				}

				strResult = strphannguyen.substring(i, i + 1) + strResult;
				tam = tam + 1;
			}
			strResult = strResult + strphanthapphan;
		}
		return strResult;
	}

	function GetNumber(str) {
		var count = 0;
		for (var i = 0; i < str.length; i++) {
			var temp = str.substring(i, i + 1);
			if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9))) {
				alert(inputnumber);
				return str.substring(0, i);
			}
			if (temp == " ")
				return str.substring(0, i);
			if (temp == ".") {
				if (count > 0)
					return str.substring(0, ipubl_date);
				count++;
			}
		}
		return str;
	}

	function IsNumberInt(str) {
		for (var i = 0; i < str.length; i++) {
			var temp = str.substring(i, i + 1);
			if (!(temp == "." || (temp >= 0 && temp <= 9))) {
				alert(inputnumber);
				return str.substring(0, i);
			}
			if (temp == ",") {
				return str.substring(0, i);
			}
		}
		return str;
	}
</script>

@endsection

@section('style')

<style>
    
</style>
    
@endsection