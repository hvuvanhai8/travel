@extends('admin.layout.master')

@section('content') 
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li>Quản lý tour du lịch</li>
                <li class="active">Danh sách các tour</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">Danh sách tất cả các tour</h3>
    
                <div class="box-tools">
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
    
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                    </div>
                </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                    <th style="width: 40px;">STT</th>
                    <th style="width: 90px;">Hình ảnh</th>
                    <th style="width: 250px">Tên Tour</th>
                    <th>Danh mục</th>
                    <th >Trạng thái</th>
                    <th >Khởi hành</th>
                    <th >Giá tour</th>
                    <th >Ngày đi</th>
                    <th >Ngày về</th>
                    <th >Phương tiện</th>
                    <th>Hành động</th>
                    </tr>
                    @php $stt = 1; @endphp
                    @foreach ($tour as $to)
                    <tr>
                    <td>{{$stt++}}</td>
                    <td><img src="{{asset('uploadfile/tour')}}/{{$to->tour_picture}}" alt="Ảnh" width="70" height="60"></td>
                    <td class="title_t">{{$to->tour_title}}</td>
                    @foreach ($cate as $cat)
                        @if($to->tour_cat_id == $cat->id)
                            <td>Du lịch {{$cat->cat_name}}</td>
                        @endif
                    @endforeach
                    <td>
                        @if($to->tour_status == 1)
                        <span class="label label-success">Hiển thị</span>
                        @else
                        <span class="label label-danger">Đã khóa</span>
                        @endif
                    </td>
                    <td>{{$to->tour_from}}</td>
                    <td><b>{{$to->tour_price}}</b></td>
                    <td>{{$to->tour_date_go}}</td>
                    <td>{{$to->tour_date_back}}</td>
                    @foreach ($expediency as $exp)
                        @if($to->tour_expediency == $exp->id)
                        <td>{{$exp->exp_name}}</td>
                        @endif
                    @endforeach
                        <td> <a class="btn btn-success" data-toggle="modal" data-target="#modal-info-{{$to->id}}" title="chi tiết"><i class="fa fa-fw fa-info"></i></a>
                        <a class="btn btn-warning" href="{{asset('admin/tour')}}/{{$to->id}}" title="Sửa"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{$to->id}}" title="xóa"><i class="fa fa-trash"></i></a></td>
                    </tr>

                    <div class="modal fade in delete_tour" id="modal-delete-{{$to->id}}" style="display: none;">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                              <h4 class="modal-title">Bạn có chắn chắn muốn xóa tour này ?</h4>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy bỏ</button>
                                <a href="{{asset('admin/tour/delete')}}/{{$to->id}}" class="btn btn-danger">Xóa bỏ</a>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="modal fade in" id="modal-info-{{$to->id}}" style="display: none;">
                        <div class="modal-dialog modal_info">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                              <h4 class="modal-title">{{$to->tour_title}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="header_body">
                                    <div class="img_tour"><img src="{{asset('uploadfile/tour')}}/{{$to->tour_picture}}" alt="Ảnh" width="250" height="230"></div>
                                    <div class="info_tour">
                                        <p class="price">Giá tour : <b>{{$to->tour_price}}</b><sup> đ</sup></p>
                                        <p class="price">Giá tour khuyến mãi: <b>{{$to->tour_price_pro}}</b><sup> đ</sup></p>
                                        <p class="price_dis">Giá giảm trừ : <span>{{$to->tour_price_discount}} <sup>đ</sup></span></p>
                                        <p class="price">Giá trẻ em (VN) : <b>{{($to->tour_price_child == 0 || $to->tour_price_child == null  ? 'Liên hệ':$to->tour_price_child."<sup> đ</sup>")}}</b></p>
                                        <p class="price">Giá em bé (VN) : <b>{{($to->tour_price_baby == 0 || $to->tour_price_baby == null ? 'Liên hệ':$to->tour_price_baby."<sup> đ</sup>")}}</b></p>
                                        <p class="price">Giá người nước ngoài : <b>{{($to->tour_price_foreign == 0 || $to->tour_price_foreign == null ? 'Liên hệ':$to->tour_price_foreign."<sup> đ</sup>")}}</b></p>
                                        <p class="price">Giá trẻ em nước ngoài : <b>{{($to->tour_price_child_foreign == 0 || $to->tour_price_child_foreign == null ? 'Liên hệ':$to->tour_price_child_foreign."<sup> đ</sup>")}}</b></p>
                                        <p class="price">Giá em bé nước ngoài : <b>{{($to->tour_price_baby_foreign == 0 || $to->tour_price_baby_foreign == null ? 'Liên hệ':$to->tour_price_baby_foreign."<sup> đ</sup>")}}</b></p>
                                        <p class="price">Giá phụ thu phòng đơn : <b>{{$to->tour_price_single_room == null ? '0':$to->tour_price_single_room}} đ</b></p>
                                    </div>
                                    <div class="info_tour">
                                        <p class="price">Hướng dẫn viên : <b>Không có</b></p>
                                        <p class="price">Ngày đi : <b>{{$to->tour_date_go}}</b></p>
                                        <p class="price">Ngày về : <b>{{$to->tour_date_back}}</b></p>
                                        <p class="price">Thời gian : <b>{{$to->tour_time}}</b></p>
                                        <p class="price">Phương tiện : 
                                            @foreach ($expediency as $exp)
                                                @if($to->tour_expediency == $exp->id)
                                                <b>{{$exp->exp_name}}</b>
                                                @endif
                                            @endforeach
                                        </p>
                                        <p class="price">Địa điểm chính : 
                                            @foreach ($place as $pla)
                                                @if($to->tour_pla_id == $pla->id)
                                                <b>{{$pla->pla_name}}</b>
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                    <div class="info_tour">
                                        @if($to->tour_hot == 1)
                                        <p class="price">Tour nổi bật : <i style="color: green;" class="fa fa-fw fa-check"></i></p>
                                        @else
                                        <p class="price">Tour nổi bật : <i class="fa fa-fw fa-close"></i></p>
                                        @endif
                                        <p class="price">Khởi hành từ : <b>{{$to->tour_from}}</b></p>
                                        <p class="price">Số chỗ trống : <b>{{$to->tour_blank}}</b></p>
                                        <div class="ad_tour">
                                            @foreach ($users as $user)
                                            @if($to->tour_user_id == $user->id)
                                            <p>Người tạo tour : {{$user->name}}</p>
                                            @endif
                                            @endforeach
                                            <p>Ngày tạo tour : {{$to->created_at}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="des_body">
                                    @foreach ($cate as $cat)
                                    @if($to->tour_cat_id == $cat->id)
                                    <div class="title_tour_cat"><h4>Danh mục : {{$cat->cat_name}}</h4></div>
                                    @endif
                                    @endforeach
                                    <div class="title_tour_description"><h4>Nội dung chi tiết</h4></div>
                                    <div class="main_tour">{!!$to->tour_description!!}</div> 
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                              <a href="{{asset('admin/tour')}}/{{$to->id}}" class="btn btn-primary">Sửa lại</a>
                            </div>
                          </div>
                        </div>
                    </div>
                    @endforeach
                </tbody></table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-primary" href="{{route('tour.create')}}">Thêm</a>
                    <div class="paginate_cate text-center">
                            {{$tour->links()}}
                    </div>
                </div>
            </div>
            <!-- /.box -->
            </div>
        </div>
    </section>
@endsection