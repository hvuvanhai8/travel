@php
//  dd($cat_nn);   
@endphp

<footer>
    <div class="bottom_bg">
        <img src="{{asset('web')}}/images/bottombg.png" alt="">
    </div>

    <div class="bottom_list_place">
        <div class="container">
            <div class="row">
                <div class="col-md-2 info_rs">
                    <h6 class="tit_rs">Thông tin hữu ích</h6>
                    <span class="cbg"></span>
                    <ul class="list_rs">
                        <li><a href="#">Hướng dẫn đặt tour</a></li>
                        <li><a href="#">Hình thức thanh toán</a></li>
                        <li><a href="#">Chính sách hoàn hủy</a></li>
                        <li><a href="#">Điều khoản sử dụng</a></li>
                        <li><a href="#">Liên hệ</a></li>
                    </ul>
                </div>
                <div class="col-md-5 info_rs">
                    <h6 class="tit_rs">Du lịch trong nước</h6>
                    <span class="cbg"></span>
                    <ul class="list_rs tour_tn_bt">
                        @foreach($cat_tn as $ca)
                        <li><a href="/du-lich-tai/{{$ca->id}}/{{$ca->cat_slug}}.html">{{$ca->cat_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-5 info_rs">
                    <h6 class="tit_rs">Du lịch nước ngoài</h6>
                    <span class="cbg"></span>
                    <ul class="list_rs tour_tn_bt">
                        @foreach($cat_nn as $ca)
                        <li><a href="/du-lich-tai/{{$ca->id}}/{{$ca->cat_slug}}.html">{{$ca->cat_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="info_company">
        <div class="container">
            <div class="row">
                <div class="col-md-4 info_cp">
                    <h6 class="info_rs">Liên hệ</h6>
                    <span class="cbg"></span>
                    <ul class="ul_inf">
                        <li><b>Văn phòng chính</b><p>129 Nguyễn Huệ, P. Bến Nghé, Q.1, TP.HCM Điện thoại: (028) 39 14 14 14 - Fax: (028) 39 14 13 63</p></li>
                        <li>
                            <b>Điểm kinh doanh</b>
                            <p>87 Phan Kế Bính, Q.1, TP.HCM Điện thoại: (028) 39 14 15 16</p>
                        </li>
                        <li>Email: fidi@gmail.com</li>
                        <li>Nguồn: wwww.fiditour.com</li>
                    </ul>
                </div>
                <div class="col-md-4 logo_ft">
                    <img src="{{asset('web')}}/images/logo/luhanhfiditour.png" alt="logo">
                </div>
                <div class="col-md-4">
                    <div class="ct_info_r">
                        <b>Thương hiệu du lịch lữ hành uy tín hàng đầu tại Việt Nam	Công ty Cổ Phần Lữ Hành Fiditour là công ty du lịch</b> luôn đi tiên phong trong việc đưa ra những chương trình khám phá tour tuyến mới lạ, độc đáo, cũng như những bước đột phá trong tổ chức dịch vụ du lịch.
                    </div>
                    <div class="socical_ct">
                        <a href="#"><i class="fab fa-facebook-square"></i></a>
                        <a href="#"><i class="fab fa-youtube-square"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-twitter-square"></i></a>
                    </div>
                </div>
            </div>
            <div class="row bt_line">
                <p class="p_pt">
                    Bản quyền thuộc Lữ Hành Fiditour ® 2019. Bảo lưu mọi quyền. Ghi rõ nguồn "www.fiditour.com" ® khi sử dụng lại thông tin từ website này Số đăng ký kinh doanh: 0315532382 do Sở kế hoạch và đầu tư TPHCM cấp.	
                </p>
            </div>
        </div>
    </div>
</footer>