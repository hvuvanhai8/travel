<header>
    <div class="top_bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 left_bar">
                    <b class="b_txt">Lữ hành Fiditour - Official website</b>
                </div>
                <div class="col-md-6 right_bar">
                    <div class="info_bar">
                        <a href="#">giới thiệu</a>
                        <a href="#">Điều khoản</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 logo">
                    <a href="/"><img src="{{asset('web')}}/images/logo/luhanhfiditour.png" alt="logo"></a>
                </div>
                <div class="col-md-6  banner_top">
                    <a href="#"><img src="{{asset('web')}}/images/banner/banner_top.png" alt="banner"></a>
                </div>
                <div class="col-md-3 search_top">
                    <form method="post" action="{{route('seach.tour')}}" class="form_search_top">
                        @csrf
                        <input type="text" name="keyword" class="form-control" placeholder="Nhập từ khóa cần tìm">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                    <div class="regis_mail">
                        <a href="#"><i class="far fa-thumbs-up"></i>Đăng ký đại lý</a>
                        <a href="#"><i class="fas fa-envelope"></i>Thư góp ý</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>