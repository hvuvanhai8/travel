<!DOCTYPE html>
<html>
<head>
	<title>Đặt Tour du lịch trong nước - nước ngoài</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/web/css/style_all.css{{env('APP_ENV')=='local'? '?v='.time():'' }}">
	
</head>
<body>

	<!-- header -->
    @include('web.layout.header')
	<!-- end header -->
    
    @include('web.layout.menu')
	
    @yield('content')
	<!-- end main -->

	<!-- footer -->
	@include('web.layout.footer')
	<!-- end footer -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
	<script type="text/javascript" src="/web/js/js_all.js"></script>

	<script>
		$(document).ready(function(){
			$('input[type=radio][name=tourtype]').on('change', function() {
			var id = $(this).val();
			switch(id) {
				case '1':
				$.get("/ajax/loaitin/"+id,function(data){
					$("#filter1").html('');
					$("#filter1").append('<option value="0">Chọn điểm đến</option>');
					$("#filter1").append(data);
				});
				break;
				case '0':
				$.get("/ajax/loaitin/"+id,function(data){
					$("#filter1").html('');
					$("#filter1").append('<option value="0">Chọn điểm đến</option>');
					$("#filter1").append(data);
				});
				break;
			}
			});
		});
	</script>

	@yield('script')

	
	
</body>
</html>