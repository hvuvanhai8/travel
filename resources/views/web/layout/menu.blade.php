@php
// dd($place);
@endphp

<div id="" class="menu">
    <div class="main_menu">
        <div class="container">
            <ul class="nav_bar">
                <li class="menu_p"><a href="/" title="trang chủ" class="home_item"><i class="fas fa-home"></i></a></li>
                @foreach($cate_p as $ca_p)
                @if($ca_p->cat_parent_id == 0 && $ca_p->cat_type == "trongnuoc" || $ca_p->cat_type == "nuocngoai")
                <li class="menu_p"><a href="/du-lich/{{$ca_p->id}}/{{$ca_p->cat_slug}}.html">{{$ca_p->cat_name}}<i class="fas fa-caret-down"></i></a>
                  <ul class="menu_child">
                    @foreach($cate_c as $ca_c)
                      @if($ca_c->cat_parent_id == $ca_p->id)
                      <li><a href="/du-lich-tai/{{$ca_c->id}}/{{$ca_c->cat_slug}}.html">{{$ca_c->cat_name}}</a></li>
                      @endif
                    @endforeach
                  </ul>
                </li>
                @endif
                @endforeach

                @foreach($cate_p as $ca_p)
                @if($ca_p->cat_parent_id == 0 && $ca_p->cat_type == "doanhnghiep")
                <li class="menu_p"><a href="/du-lich/tour-doanh-nghiep.html">{{$ca_p->cat_name}}</a>
                </li>
                @endif
                @endforeach

                @foreach($cate_p as $ca_p)
                @if($ca_p->cat_parent_id == 0 && $ca_p->cat_type == "tintuc")
                <li class="menu_p"><a href="/tin-tuc/{{$ca_p->cat_slug}}">{{$ca_p->cat_name}}<i class="fas fa-caret-down"></i></a>
                  <ul class="menu_child">
                    @foreach($cate_c as $ca_c)
                      @if($ca_c->cat_parent_id == $ca_p->id)
                      <li><a href="/tin-tuc/{{$ca_c->cat_slug}}">{{$ca_c->cat_name}}</a></li>
                      @endif
                    @endforeach
                  </ul>
                </li>
                @endif
                @endforeach
                <li class="menu_p"><a href="#">liên hệ</a></li>
            </ul>
        </div>
        <div class="filter_tour">
            <div class="fil_cap"><span><i class="fas fa-map-signs"></i> TÌM TOUR DU LỊCH</span></div>
            <div class="triangle"></div>
            <div class="fil_main">
                <form action="/loc-tour" class="form_filter" method="post">
                    @csrf
                    <div class="form-check">
                        <label class="form-check-label" for="radio1">
                          <input type="radio" class="form-check-input" id="trongnuoc" name="tourtype" value="1" checked>Trong nước
                        </label>
                      </div>
                      <div class="form-check">
                        <label class="form-check-label" for="radio2">
                          <input type="radio" class="form-check-input" id="nuocngoai" name="tourtype" value="0">Nước ngoài
                        </label>
                      </div>
                      <select class="form-control form_one" id="filter1" name="pla_id">
                        <option value="0">Chọn điểm đến</option>
                        @foreach($place as $pla)
                        <option value="{{$pla->id}}">{{$pla->pla_name}}</option>
                        @endforeach
                      </select>
                      <select class="form-control form_two" id="filter2">
                        <option value="0">Tất cả</option>
                      </select>
                      <button type="submit" class="btn_submit">
                        Tìm kiếm
                        <span class="trian_left"></span>

                      </button>
                      <span class="trian_right"></span>
                </form>
            </div>
        </div>
    </div>
</div>









