

@extends('web.layout.master')

@section('content') 

{{-- @include('web.layout.menu') --}}

<div class="container book_tour">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb_t">
                <a href="/" title="trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a title="trang chủ"> Đặt tour <i class="fas fa-angle-right"></i></a>
                <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}"> {{$tour_detail->tour_title}}</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tit_detail">
                <h4 class="title_tt">ĐẶT TOUR</h4>
                <span class="cbg"></span>
            </div>
            <div class="bg_panel">
                Để đặt tour. Quý khách vui lòng hoàn tất các bước sau.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Nhập thông tin</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle">
                        <span class=""></span>
                    </button>
                    <p>Khách hàng xác nhận</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle">
                        <span class=""></span>
                    </button>
                    <p>Đặt tour thành công</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="book_tour_img">
                <img src="{{asset('uploadfile')}}/tour/{{$tour_detail->tour_picture}}" alt="ảnh">
            </div>
        </div>
        <div class="col-md-8">
            <div class="tour_info">
                <table class="table table-bordered" id="tbl_tourdetail">
                    <thead>
                        <tr>
                            <th colspan="2" align="left" style="text-align: left;">
                            <span class="tit_name">{{$tour_detail->tour_title}}</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody_full">
                        <tr>
                            <td>
                                Mã tour
                            </td>
                            <td>
                                {{$tour_detail->tour_code}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ngày khởi hành
                            </td>
                            <td>
                                {{date("d-m-Y", strtotime($tour_detail->tour_date_go))}}
                            </td>
                        </tr>
                        <tr>
                            <td>Thời gian đi</td>
                            <td>
                                {{$tour_detail->tour_time}}
                            </td>
                        </tr>
                        <tr>
                            <td>Phương tiện:</td>
                            <td>
                                @foreach($expediency as $ex)
                                    @if($tour_detail->tour_expediency == $ex->id)
                                        <span class="depdate  pt_maybay"></span>{{$ex->exp_name}}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Giá</td>
                            <td>
                                {{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}}
                            </td>
                        </tr>
                        <tr>
                            <td>Số chỗ nhận</td>
                            <td>
                                {{$tour_detail->tour_blank}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="order_tbl_price">
                <div class="tt_order">
                    GIÁ TOUR
                </div>
                <table class="table table-bordered table-striped tableRefix">
                    <thead>
                        <tr>
                            <th>Quốc tịch</th>
                            <th>Người Lớn</th>
                            <th>Trẻ Em</th>
                            <th>Em Bé</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Việt Nam</td>
                            <td data-title="Người lớn:">
                                {{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}} đ                                                        
                            </td>
                            <td data-title="Trẻ em:">
                                <span>{{($tour_detail->tour_price_child == 0 || $tour_detail->tour_price_child == null  ? 'Liên hệ':$tour_detail->tour_price_child.' đ')}}</span>
                            </td>
                            <td data-title="Em bé:">
                                <span>{{($tour_detail->tour_price_baby == 0 || $tour_detail->tour_price_baby == null  ? 'Liên hệ':$tour_detail->tour_price_baby.' đ')}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Nước Ngoài</td>
                            <td data-title="Người lớn:">
                                <span>{{($tour_detail->tour_price_foreign == 0 || $tour_detail->tour_price_foreign == null  ? 'Liên hệ':$tour_detail->tour_price_foreign.' đ')}}</span>
                            </td>
                            <td data-title="Trẻ em:">
                                <span>{{($tour_detail->tour_price_child_foreign == 0 || $tour_detail->tour_price_child_foreign == null  ? 'Liên hệ':$tour_detail->tour_price_child_foreign.' đ')}}</span>
                            </td>
                            <td data-title="Em bé:">
                                <span>{{($tour_detail->tour_price_baby_foreign == 0 || $tour_detail->tour_price_baby_foreign == null  ? 'Liên hệ':$tour_detail->tour_price_baby_foreign.' đ')}}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table_single">
                    <tbody>
                        <tr>
                            <td colspan="4">Phụ thu phòng đơn</td>
                            <td>{{$tour_detail->tour_price_single_room == null ? '0':$tour_detail->tour_price_single_room}} đ</td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-4">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger" style="margin-top: 10px;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
                <form class="form-horizontal cofrm" action="{{asset('/dat-tour')}}/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html?page=nhapthongtin&code={{$tour_detail->tour_code}}" enctype="multipart/form-data" id="ordertour" method="post" novalidate="novalidate"> 
                    @csrf
                    <div class="div_order">
                        <div class="text-center">
                            <div class="tit_order">
                                THÔNG TIN LIÊN HỆ
                            </div>
                        </div>
                        <div class="form_order row">
                            <div class="col-md-6" style="padding-bottom: 10px;">
                                <label>Họ và tên <span style="color: #cd2c24">*</span></label>
                                <div>
                                    <input class="form-control" id="hoten" name="cus_name" required type="text">
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 10px;">
                                <label>Số điện thoại <span style="color: #cd2c24">*</span></label>
                                <div>
                                    <input class="form-control" id="dienthoai" onkeypress="return isNumberKey(event)" name="cus_phone" required type="text">
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 10px;">
                                <label>Email <span style="color: #cd2c24">*</span></label>
                                <div>
                                    <input class="form-control" id="email" name="cus_email" required type="text">
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 10px;">
                                <label>Địa chỉ <span style="color: #cd2c24">*</span></label>
                                <div>
                                    <input class="form-control" id="diachi" name="cus_address" required type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div_order">
                        <div class="text-center">
                            <div class="tit_order soluong_ditour" id="3">
                                SỐ LƯỢNG NGƯỜI ĐI TOUR
                            </div>
                        </div>
                        <div class="form_order row">
                            <div class="col-md-3">
                                <label>Người lớn <span style="color: #cd2c24">*</span></label>
                                <div>
                                    <input class="form-control sokhdi" id="sl_nguoilon" onkeypress="return isNumberKey(event)" name="nguoilon" required="required" type="text" value="1">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Trẻ em</label>
                                <div>
                                    <input class="form-control sokhdi" id="sl_treem" onkeypress="return isNumberKey(event)" name="treem" type="text" value="0">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Em bé</label>
                                <div>
                                    <input class="form-control sokhdi" id="sl_embe" onkeypress="return isNumberKey(event)" name="embe" type="text" value="0">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Tổng</label>
                                <div>
                                    <input class="form-control" id="tong_nguoidi" onkeypress="return isNumberKey(event)" name="tong_nguoidi" type="text" readonly value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div_next">
                        <button type="submit" class="btn-next" id="tieptheo">Tiếp theo <i class="fas fa-chevron-circle-right"></i></button>                     
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

        var tour_bank = "@php echo $tour_detail->tour_blank;  @endphp";
        var int_tour_bank = Number(tour_bank);
        var error_book = "Số chỗ nhận là @php echo $tour_detail->tour_blank;  @endphp";
        // console.log(int_tour_bank);
        $(document).ready(function(){
            var a = 1;
            var b = 0;
            var c = 0;
            $('#sl_nguoilon').change(function(){
                var sl_nl = $( this ).val();
                a = sl_nl;
                // console.log(a);
                tong(a,b,c);
            });
            $('#sl_treem').change(function(){
                var sl_te = $( this ).val();
                b = sl_te;
                tong(a,b,c);
            });
            $('#sl_embe').change(function(){
                var sl_eb = $( this ).val();
                c = sl_eb;
                tong(a,b,c);
            });
            function tong(a,b,c){
                var tong = Number(a) + Number(b) + Number(c);
                console.log(tong);
                if(tong > int_tour_bank){
                    
                    $('#sl_nguoilon').val(1);
                    $('#sl_treem').val(0);
                    $('#sl_embe').val(0);
                    $("#tong_nguoidi").val(1);
                    a = 1;
                    b = 0;
                    c = 0;
                    alert(error_book);
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }else{
                    $("#tong_nguoidi").val(tong);
                }
                // console.log(tong);
               
            }
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if(charCode == 59 || charCode == 46)
                return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
       
    
        
</script>


@endsection