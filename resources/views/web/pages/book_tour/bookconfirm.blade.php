

@extends('web.layout.master')

@section('content') 

{{-- @include('web.layout.menu') --}}

<div class="container book_tour">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb_t">
                <a href="/" title="trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a title="trang chủ"> Đặt tour <i class="fas fa-angle-right"></i></a>
                <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}"> {{$tour_detail->tour_title}}</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tit_detail">
                <h4 class="title_tt">ĐẶT TOUR</h4>
                <span class="cbg"></span>
            </div>
            <div class="bg_panel">
                Để đặt tour. Quý khách vui lòng hoàn tất các bước sau.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Nhập thông tin</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Khách hàng xác nhận</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle">
                        <span class=""></span>
                    </button>
                    <p>Đặt tour thành công</p>
                </div>
            </div>
            <div style="width:100%; border: dotted 1px #999; background-color: #fff6c2; margin-bottom:10px; padding: 5px;"><span style="color: red;">
                Quý khách vui lòng kiểm tra lại thông tin và chọn hình thức thanh toán để hoàn tất đơn đặt hàng.
            </span></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="book_tour_img">
                <img src="{{asset('uploadfile')}}/tour/{{$tour_detail->tour_picture}}" alt="ảnh">
            </div>
        </div>
        <div class="col-md-8">
            <div class="tour_info">
                <table class="table table-bordered" id="tbl_tourdetail">
                    <thead>
                        <tr>
                            <th colspan="2" align="left" style="text-align: left;">
                            <span class="tit_name">{{$tour_detail->tour_title}}</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody_full">
                        <tr>
                            <td>
                                Mã tour
                            </td>
                            <td>
                                {{$tour_detail->tour_code}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ngày khởi hành
                            </td>
                            <td>
                                {{date("d-m-Y", strtotime($tour_detail->tour_date_go))}}
                            </td>
                        </tr>
                        <tr>
                            <td>Thời gian đi</td>
                            <td>
                                {{$tour_detail->tour_time}}
                            </td>
                        </tr>
                        <tr>
                            <td>Phương tiện:</td>
                            <td>
                                @foreach($expediency as $ex)
                                    @if($tour_detail->tour_expediency == $ex->id)
                                        <span class="depdate  pt_maybay"></span>{{$ex->exp_name}}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Giá</td>
                            <td>
                                {{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}}
                            </td>
                        </tr>
                        <tr>
                            <td>Số chỗ nhận</td>
                            <td>
                                {{$tour_detail->tour_blank}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="div_order">
                <div class="text-center">
                    <div class="tit_order">
                        THÔNG TIN LIÊN HỆ
                    </div>
                </div>
                <table class="table table-bordered" id="tbl_tourdetail">
                    <tbody>
                        <tr>
                            <td>
                                Họ tên
                            </td>
                            <td>
                                {{$user_book->cus_name}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Điện thoại
                            </td>
                            <td> {{$user_book->cus_phone}}</td>
                        </tr>
                        <tr>
                            <td> Địa chỉ</td>
                            <td> {{$user_book->cus_address}}</td>
                        </tr>
                        <tr>
                            <td>Email  </td>
                            <td>{{$user_book->cus_email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="div_order">
                <div class="tit_tt_khachhang_coban">
                    DANH SÁCH KHÁCH ĐI TOUR
                </div>
                <table class="table table-bordered tableRefix" id="tbl_tourdetail">
                    <thead>
                        <tr class="bgcolor">
                            <th class="auto-style1">
                                Họ và tên
                            </th>
                            <th class="auto-style1">Loại khách</th>
                            <th class="auto-style1">Độ tuổi</th>
                            <th class="auto-style1">Giới tính</th>
                            <th class="auto-style1">Phòng đơn</th>
                            <th class="auto-style1">Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_cus as $cus)
                        <tr>
                            <td data-title="Họ tên:">{{$cus->cus_name}}</td>
                            <td data-title="Quốc tịch:"><span>{{$cus->cus_name}}</span></td>
                            <td data-title="Độ tuổi:"><span>{{$cus->cus_old}}</span></td>
                            <td data-title="Giới tính:"><span>{{$cus->cus_gender}}</span></td>
                            <td data-title="Phòng đơn:"><span>0 đ</span></td>
                            <td data-title="Thành tiền:"> {{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}} đ</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8" style="text-align:right;">
                                <span>Tổng cộng: </span>
                                    <input name="txt_tongcong" type="text" value="{{number_format($book->book_total)}} đ" id="txt_tongcong" disabled="disabled" class="aspNetDisabled">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="div_order">
                <div class="tit_tt_khachhang_coban">
                    HÌNH THỨC THANH TOÁN
                </div>
                <div class="row">
                    <div class="col-md-6 text-center">                                                    
                        <ul class="tttienmat">
                        <li>
                            <img src="{{asset('web')}}/images/logo/luhanhfiditour.png" title="Fiditour">
                        </li>
                        <li>
                            <input type="radio" value="0" name="banking" checked="">Thanh toán bằng tiền mặt hoặc chuyển khoản</li>
                        </ul>                           
                    </div>
                     <div class="col-md-6">
                         <ul class="vp_cn_order">
                        <li>
                        VĂN PHÒNG CÔNG TY
                        <ul>
                        <li>129 Nguyễn Huệ,  Q1, Tp.HCM</li>
                        <li>Tel: (028) 3914 1414 -  3914 1516 Fax: (028) 3914 1363</li>
                        </ul>
                        </li>
                        <li>
                        CHI NHÁNH TẠI HÀ NỘI
                        <ul>
                        <li>43A Ngô Quyền, P.Hàng Bài, Q.Hoàn Kiếm, Hà Nội</li>
                        <li>Tel: (024) 3943 4933  Fax: (024) 3943 4932</li>
                        </ul>
                        </li>
                        <li>
                        CHI NHÁNH TẠI ĐÀ NẴNG
                        <ul>
                        <li>93 Hàm Nghi, Phường Vĩnh Trung, Quận Thanh Khê, Tp.Đà Nẵng.</li>
                        <li>Tel: (0236) 399 66 33  Fax: (0236) 399 66 22</li>
                        </ul>
                        </li>
                        <li>
                        CHI NHÁNH TẠI CẦN THƠ
                        <ul>
                        <li>59 Võ Văn Tần, Phường Tân An, Quận Ninh Kiều, TP.Cần Thơ</li>
                        <li>Tel: (0292) 3687878- Fax: (0292) 3818867</li>
                        </ul>
                        </li>
                        </ul>
                     </div>
                </div>
                <br>
            </div>
            <form class="form-horizontal cofrm" action="/confirm/{{$book->id}}" enctype="multipart/form-data" id="xacnhanthanhtoan" method="post">   
            @csrf                                
            <div class="div_order">
                <div class="tit_tt_khachhang_coban">
                    ĐIỀU KHOẢN GIAO DỊCH
                </div>
                <div>
                    <div class="dieukhoangiaodich_order">
                    Điều khoản này là sự thỏa thuận đồng ý của quý khách khi sử dụng dịch vụ thanh toán trên các trang web www.fiditour.com, www.fiditour.vn của Công Ty Cổ Phần Lữ Hành Fiditour và những trang web của bên thứ ba (là những đơn vị liên kết với Fiditour: 123pay, nhằm hỗ trợ việc thanh toán qua mạng cho quý khách). Việc quý khách nhấp chuột vào nút “Đồng ý” là quý khách đã chấp nhận đồng ý tất cả các điều khoản thỏa thuận trong các trang web này.

                    <ul>
                    <li>
                    1.Về sở hữu bản quyền:
                    <ul>
                    <li>
                    Tất cả các trang web www.fiditour.com , www.fiditour.vn đều thuộc quyền sở hữu của Fiditour và được bảo vệ theo luật bản quyền, quý khách chỉ được sử dụng  các trang web này với mục đích xem thông tin và đăng ký thanh toán online cho cá nhân chứ không được sử dụng cho bất cứ mục đích thương mại nào khác.
                    Việc lấy nội dung để tham khảo, làm tài liệu cho nghiên cứu phải ghi rõ ràng nguồn lấy từ nội dung trang web Fiditour. Không được sử dụng các logo, các nhãn hiệu Fiditour dưới mọi hình thức nếu chưa có sự đồng ý của Fiditour bằng văn bản.
                    </li>
                    </ul>
                    </li>
                    <li>
                    2.Về thông tin khách hàng
                    <ul>
                    <li>
                    Khi đăng ký thanh toán qua mạng, quý khách sẽ được yêu cầu cung cấp một số thông tin cá nhân và thông tin tài khoản.
                    <ul>
                    <li>
                        -Đối với thông tin cá nhân: Những thông tin này chỉ để phục vụ cho nhu cầu xác nhận sự mua tour của quý khách và sẽ hiển thị những nội dung cần thiết trên các mẫu đơn thanh toán điện tử khi giao dịch. Fiditour cũng sẽ sử dụng những thông tin liên lạc này để gửi đến quý khách những thông tin về sự kiện, khuyến mãi và những ưu đãi đặc biệt. Những thông tin này của quý khách sẽ được Fiditour giữ bí mật và không tiết lộ cho bên thứ ba biết ngoại trừ sự đồng ý của quý khách hoặc là phải tiết lộ theo sự tuân thủ luật pháp quy định.


                    </li>
                    <li>
                        -Đối với thông tin tài khoản: Những thông tin này sẽ được Fiditour và bên thứ ba (123pay) áp dụng những biện pháp bảo mật cao nhất do các hệ thống thanh toán nổi tiếng trên thế giới như Visa và Master Card cung cấp nhằm đảm bảo sự an toàn tuyệt đối của thông tin tài khoản quý khách.
                    </li>
                    </ul>
                    </li>

                    </ul>
                    </li>
                    <li>
                    3.Về trang web liên kết
                    <ul>
                    <li>
                    Các trang web của Fiditour có chứa những liên hệ kết nối với trang web của bên thứ ba (123pay). Việc liên kết trang web của bên thứ ba này nhằm chỉ cung cấp những tiện lợi cho quý khách trong việc thanh toán qua mạng, chứ không phải là sự tán thành, chấp nhận những nội dung, thông tin sản phẩm của những trang web bên thứ ba. Fiditour sẽ không chịu trách nhiệm về bất cứ trách nhiệm pháp lý nào liên quan đến những thông tin gì trong các trang web bên thứ ba.
                    </li>
                    </ul>
                    </li>
                    <li>
                    4.Trách nhiệm của Fiditour
                    <ul>
                    <li>
                    - Fiditour có nhiệm vụ bảo mật và lưu trữ an toàn các thông tin của quý khách với sự nghiêm túc cao nhất.
                    </li>
                    <li>- Giải quyết những thắc mắc, sai sót, vi phạm mà quý khách có được trong quá trình thanh toán nếu do lỗi của Fiditour.</li>
                    <li>- Hoàn tất các thủ tục liên quan đến chuyến du lịch, tổ chức tour du lịch đúng theo chương trình đã thỏa thuận. Tuy nhiên chúng tôi có toàn quyền thay đổi lộ trình, phương tiện vận chuyển, điểm tham quan, hủy bỏ một số tiết mục… hoặc hủy bỏ chuyến du lịch bất cứ lúc nào mà chúng tôi thấy cần thiết vì sự an toàn cho quý khách.</li>
                    <li>- Có nghĩa vụ mua bảo hiểm du lịch cho quý khách. Việc đền bù các tổn thất áp dụng theo quy định của nhà cung cấp dịch vụ bảo hiểm.</li>
                    <li> - Được quyền thay đổi thứ tự chương trình tham quan du lịch cho phù hợp với tình hình thực tế, và sẽ cố gắng thông báo nhanh nhất cho quý khách các thay đổi nếu có.</li>
                    <li>- Fiditour sẽ không chịu trách nhiệm về mọi chi phí do quý khách tự ý bỏ, chậm trễ, thay đổi chương trình tour (một phần hay toàn bộ) hoặc sử dụng dịch vụ thêm.</li>
                    </ul>
                    </li>
                    <li>
                    5.Trường hợp miễn trách nhiệm với Fiditour
                    <ul>
                    <li>- Fiditour sẽ không chịu trách nhiệm về tất cả những thông tin mà quý khách cung cấp, do chúng tôi không dễ dàng xác nhận chính xác quý khách có phải là chủ thẻ thanh toán đăng ký thông tin.</li>
                    <li>- Fiditour không chịu trách nhiệm về việc thông tin của quý khách bị lấy cắp, nếu như việc lấy cắp được thực hiện từ máy của quý khách do bị nhiễm virus máy tính hay do nguyên nhân nào khác.</li>
                    <li>- Fiditour sẽ không chịu trách nhiệm đối với quý khách nếu xảy ra việc hệ thống máy tính của quý khách bị hư hại trong khi đang thanh toán, hoặc bị can thiệp liên qua đến việc sử dụng một trang khác.</li>
                    <li>- Fiditour cũng không chịu trách nhiệm về việc mất dữ liệu thông tin của quý khách do sự cố khách quan như: thiên tai, hạn hán, hỏa hoạn, chiến tranh…</li>
                    <li>- Fiditour được miễn trách nhiệm trong quá trình thực hiện tour nếu xảy ra các trường hợp bất khả kháng như: tình hình bất ổn chính trị-xã hội, quyết định của chính quyền về khu vực tham quan bị phong tỏa, về cấp thị thực xuất nhập cảnh và chi phí liên quan, chiến tranh, thiên tai, dịch bệnh, sự thay đổi việc cung ứng dịch vụ của các nhà cung cấp…Tùy từng trường hợp cụ thể, hai bên Fiditour và quý khách cùng nhau bàn bạc và tìm ra phương án tối ưu để giải quyết nhằm giảm thiệt hại ở mức thấp nhất.</li>
                    </ul>
                    </li>
                    <li>
                    6.Quy định hủy tour
                    <ul>
                    <li>- Trường hợp quý khách hủy tour trước ngày khởi hành, quý khách vui lòng gởi email thông báo hủy tour đến Fiditour, chúng tôi sẽ trao đổi và xác nhận lại tất cả các thông tin của quý khách. Khi hoàn tất việc xác nhận thông tin, Fiditour sẽ hoàn tiền (nếu có) vào đúng tài khoản quý khách đã thanh toán sau khi trừ các khoản lệ phí hủy tour. Lệ phí hủy tour theo quy định của chương trình tour mà quý khách đăng ký.</li>
                    <li>- Fiditour sẽ không hoàn tiền cho quý khách trong trường hợp quý khách đăng ký mua tour và đã thanh toán cho Fiditour, nhưng quý khách không đi tour.</li>
                    </ul>
                    </li>
                    <li>
                    7.Trách nhiệm của khách hàng
                    <ul>
                    <li>- Quý khách cam kết hoàn toàn chịu trách nhiệm về các thông tin cá nhân, thông tin thẻ tín dụng đã được khai báo là trung thực, chính xác. Nếu có sai sót, giả mạo hay tranh chấp phát sinh thì Fiditour có quyền hủy tour đã mua của quý khách, hoặc không hoàn tiền lại cho quý khách nếu Fiditour đã phục vụ tour cho người đăng ký và những người liên quan đến việc đăng ký, sử dụng tour.</li>

                    <li>
                    - Quý khách đến văn phòng Công ty Fiditour ký cam kết mua tour dùm cho người khác, trong trường hợp quý khách dùng thẻ thanh toán của mình để thanh toán tiền tour cho người đó.
                    </li>

                    <li> -  Quý khách không sử dụng các nội dung của các trang web do Fiditour quản lý cho mục đích thương mại nếu như chưa có sự đồng ý.</li>

                    <li>  - Quý khách phải tự áp dụng cài đặt các biện pháp phòng ngừa để đảm bảo rằng, bất cứ lựa chọn nào của quý khách khi sử dụng các trang web của Fiditour, không bị virus hoặc bất cứ mối đe dọa nào khác từ ngoài có thể can thiệp hoặc gây hư hại cho hệ thống máy tính của quý khách.</li>

                    <li>  - Quý khách chịu mọi chi phí phát sinh do việc cung cấp thông tin thiếu, không chính xác.</li>

                    <li>  - Không tự ý tách đoàn nếu chưa được sự đồng ý của trưởng đoàn bên Fiditour. Tuân thủ các quy định về an toàn cá nhân. Tự bảo quản hành lý và tư trang của mình. Mang theo giấy tờ tùy thân để xuất trình khi cần thiết.</li>
                    <li>
                    - Thanh toán cho Fiditour đầy đủ, đúng thời hạn theo thỏa thuận giữa hai bên và chịu mọi chi phí phát sinh do chọn lựa thêm ngoài chương trình.
                    </li>

                    <li> - Chấp hành pháp luật của nhà nước Việt Nam (và các nước sở tại nếu đi du lịch nước ngoài), thực hiện nội quy của đoàn, quy định của khách sạn, của phương tiện vận chuyển, của điểm tham quan… Không được mang theo hàng quốc cấm, vũ khí, chất nổ… trong suốt quá trình đi du lịch. Chịu trách nhiệm đền bù toàn bộ do gây thiệt hại vật chất, và các khoản phạt do vi phạm luật pháp, và chi phí hủy dịch vụ của nhà cung cấp</li>

                    <li> - Sau khi đi tour với Fiditour, Quý khách không được từ chối thanh toán vì bất kỳ lý do khiếu nại nào.</li>

                    <li> - Khi đã đọc các điều kiện thanh toán trực tuyến trên website Fiditour và đã đồng ý các điều kiện trên, quý khách sẽ không được từ chối thanh toán vì bất kỳ lý do gì.</li>
                    </ul>
                    </li>

                    </ul>
                    </div>
                            </div>
                        </div>
                <div class="div_order">
                    <div class="tit_tt_khachhang_coban">
                    </div>                                          
                        <div class="checkbox dydieukhoan">
                            <label class="">
                                <input id="dongy_dieukhoan" type="checkbox" name="dongy_dieukhoan">
                                Tôi đã đọc và đồng ý với các điều khoản thanh toán của FIDITOUR
                            </label>
                        </div>
                    <div class="tit_tt_khachhang_coban">
                    </div>
                </div>
                <div class="div_next" style="display: none;">
                    <button type="submit" id="bt_thanhtoan" style="display: block;">Thanh toán</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    $(document).ready(function () {
      $('#dongy_dieukhoan').click(function () {
        if ($(this).prop('checked')) {
           $('.div_next').fadeIn();
        }
        else {       
            $('.div_next').fadeOut();
        }
      });
  });
</script>


@endsection