

@extends('web.layout.master')

@section('content') 

{{-- @include('web.layout.menu') --}}

<div class="container book_tour">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb_t">
                <a href="/" title="trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a title="trang chủ"> Đặt tour <i class="fas fa-angle-right"></i></a>
                <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}"> {{$tour_detail->tour_title}}</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tit_detail">
                <h4 class="title_tt">ĐẶT TOUR</h4>
                <span class="cbg"></span>
            </div>
            <div class="bg_panel">
                Để đặt tour. Quý khách vui lòng hoàn tất các bước sau.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Nhập thông tin</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle">
                        <span class=""></span>
                    </button>
                    <p>Khách hàng xác nhận</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle">
                        <span class=""></span>
                    </button>
                    <p>Đặt tour thành công</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="div_order">
                <div class="text-center">
                    <div class="tit_order">
                        THÔNG TIN LIÊN HỆ
                    </div>
    
                </div>
                <table class="table table-bordered" id="tbl_tourdetail">
    
                    <tbody>
                        <tr>
                            <td>
                                Họ tên
                            </td>
                            <td>
                                {{$user_book->cus_name}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Điện thoại
                            </td>
                            <td> {{$user_book->cus_phone}}</td>
                        </tr>
                        <tr>
                            <td> Địa chỉ</td>
                            <td> {{$user_book->cus_address}}</td>
                        </tr>
                        <tr>
                            <td>Email  </td>
                            <td>{{$user_book->cus_email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-12">
        <form class="form-horizontal cofrm" action="{{asset('/dat-tour')}}/user={{$user_book->id}}&tong={{$total_people}}/{{$id_book}}/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html?page=nhapthongtin&code={{$tour_detail->tour_code}}" enctype="multipart/form-data" id="sendorderb2" method="post">   
                @csrf
                <div class="" style="display:none">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <td>NL</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>TE</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>TN</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>EB</td>
                            <td>0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="div_order">
                    @for($x = 1; $x <= $total_people; $x++)
                    <div>
                        <div class="tit_tt_khachhang_coban row">
                            THÔNG TIN KHÁCH HÀNG #{{$x}}
                        </div>
                        <div class="nd_giatourcoban row">
                            <div class="col-md-3">
                                <label class="mg-bot5">Họ tên (<span class="price">*</span>)</label>
                                <div>
                                    <input id="hoten_nguoidi" class="form-control" data-val="true" data-val-required="*" required="required" name="cus_name{{$x}}" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <label class="mg-bot5">Giới tính</label>
                                <div>
                                    <select class="form-control" name="cus_gender{{$x}}">
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ">Nữ</option>
                                        <option value="Khác">Khác</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <label class="mg-bot5">Độ tuổi</label>
                                <div>
                                    <select class="form-control" id="dotuoi_nguoidi" name="cus_old{{$x}}" onchange="ChangeChoose();">
                                        <option value="Người lớn">Người lớn</option>
                                        <option value="Trẻ em">Trẻ em</option>
                                        <option value="Em bé">Em bé</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <label class="mg-bot5">CMND/Hộ chiếu</label>
                                <div>
                                    <input id="pass_nguoidi" class="form-control" data-val="true" name="cus_passport{{$x}}" type="text" value="PPFD">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="mg-bot5">Ngày hết hạn</label>
                                <div>
                                    <input id="ex_pass_nguoidi" class="form-control" data-val="true" name="cus_date_passport{{$x}}" type="text" value="01/01/2020">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="mg-bot5">Quốc tịch</label>
                                <div>
                                    <select class="form-control" id="quoctich_nguoidi" name="cus_nationlity{{$x}}" onchange="ChangeChoose();">
                                        <option value="1">Việt Nam</option>
                                        <option value="0">Nước ngoài</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <label class="mg-bot5">Phòng đơn</label>
                                <div>
                                    <select class="form-control" id="phongdon_nguoidi" name="cus_single_room{{$x}}" onchange="ChangeChoose();">
                                        <option selected="selected" value="0">Không</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
                <div class="div_next">
                    <button type="submit" id="tieptheo">Tiếp theo <i class="fas fa-chevron-circle-right"></i></button>                
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

     
</script>


@endsection