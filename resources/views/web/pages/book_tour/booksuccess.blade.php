

@extends('web.layout.master')

@section('content') 

{{-- @include('web.layout.menu') --}}

<div class="container book_tour">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb_t">
                <a href="/" title="trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a title="trang chủ"> Đặt tour <i class="fas fa-angle-right"></i></a>
                <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}"> {{$tour_detail->tour_title}}</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tit_detail">
                <h4 class="title_tt">ĐẶT TOUR</h4>
                <span class="cbg"></span>
            </div>
            <div class="bg_panel">
                Đặt tour thành công !
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Nhập thông tin</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Khách hàng xác nhận</p>
                </div>
                <div class="stepwizard_row">
                    <button type="button" class="btn btn-primary btn-circle-check">
                        <span class=""><i class="fas fa-check"></i></span>
                    </button>
                    <p>Đặt tour thành công</p>
                </div>
            </div>
            <div class="noti_success">
                <p>Kính chào Quý khách!.</p>
                <p>Cảm ơn Quý khách đã quan tâm và gởi yêu cầu đặt tour của Fiditour</p> 
                <p>Chúng tôi sẽ liên hệ Quý khách trong thời giam sớm nhất để xác nhận yêu cầu đặt tour.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 style="font-weight: 600;">Sau đây là thông tin đặt tour của quý khách.</h6>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="book_tour_img">
                <img src="{{asset('uploadfile')}}/tour/{{$tour_detail->tour_picture}}" alt="ảnh">
            </div>
        </div>
        <div class="col-md-8">
            <div class="tour_info">
                <table class="table table-bordered" id="tbl_tourdetail">
                    <thead>
                        <tr>
                            <th colspan="2" align="left" style="text-align: left;">
                            <span class="tit_name">{{$tour_detail->tour_title}}</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody_full">
                        <tr>
                            <td>
                                Mã tour
                            </td>
                            <td>
                                {{$tour_detail->tour_code}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ngày khởi hành
                            </td>
                            <td>
                                {{date("d-m-Y", strtotime($tour_detail->tour_date_go))}}
                            </td>
                        </tr>
                        <tr>
                            <td>Thời gian đi</td>
                            <td>
                                {{$tour_detail->tour_time}}
                            </td>
                        </tr>
                        <tr>
                            <td>Phương tiện:</td>
                            <td>
                                @foreach($expediency as $ex)
                                    @if($tour_detail->tour_expediency == $ex->id)
                                        <span class="depdate  pt_maybay"></span>{{$ex->exp_name}}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Giá</td>
                            <td>
                                {{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="div_order">
                <div class="text-center">
                    <div class="tit_order">
                        THÔNG TIN LIÊN HỆ
                    </div>
                </div>
                <table class="table table-bordered" id="tbl_tourdetail">
                    <tbody>
                        <tr>
                            <td>
                                Họ tên
                            </td>
                            <td>
                                {{$user_book->cus_name}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Điện thoại
                            </td>
                            <td> {{$user_book->cus_phone}}</td>
                        </tr>
                        <tr>
                            <td> Địa chỉ</td>
                            <td> {{$user_book->cus_address}}</td>
                        </tr>
                        <tr>
                            <td>Email  </td>
                            <td>{{$user_book->cus_email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="div_order">
                <div class="">
                    <div class="tit_tt_khachhang_coban">
                        THÔNG TIN BOOKING
                    </div>

                </div>
                <table class="table table-bordered" id="tbl_tourdetail">
                    <tbody>
                        <tr>
                            <td>
                                Số booking
                            </td>
                            <td>
                                #{{$book->id}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trị giá booking
                            </td>
                            <td>
                                Liên hệ
                           </td>
                        </tr>
                        <tr>
                            <td> Ngày gửi</td>
                            <td>{{$book->updated_at}}</td>
                        </tr>
                        <tr>
                            <td>Hinh thức thanh toán  </td>
                            <td>

                                Thanh toán sau (Chuyển khoản/tiền mặt)
                            </td>
                        </tr>
                        <tr>
                            <td>Tình trạng thanh toán  </td>
                            <td>
                               Chưa thanh toán
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="div_order">
                <div class="tit_tt_khachhang_coban">
                    DANH SÁCH KHÁCH ĐI TOUR
                </div>
                <table class="table table-bordered tableRefix" id="tbl_tourdetail">
                    <thead>
                        <tr class="bgcolor">
                            <th class="auto-style1">
                                Họ và tên
                            </th>
                            <th class="auto-style1">Quốc tịch</th>
                            <th class="auto-style1">Độ tuổi</th>
                            <th class="auto-style1">Giới tính</th>
                            <th class="auto-style1">Phòng đơn</th>
                            <th class="auto-style1">Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_cus as $cus)
                        <tr>
                            <td data-title="Họ tên:">{{$cus->cus_name}}</td>
                            <td data-title="Quốc tịch:"><span>{{($cus->cus_nationlity == 1 ? 'Việt Nam':'Nước ngoài')}}</span></td>
                            <td data-title="Độ tuổi:"><span>{{$cus->cus_old}}</span></td>
                            <td data-title="Giới tính:"><span>{{$cus->cus_gender}}</span></td>
                            <td data-title="Phòng đơn:"><span>0 đ</span></td>
                            <td data-title="Thành tiền:">{{$tour_detail->tour_price_pro == 0 ? $tour_detail->tour_price:$tour_detail->tour_price_pro}} đ</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8" style="text-align:right;">
                                <span>Tổng cộng: </span>
                                    <input name="txt_tongcong" type="text" value="{{number_format($book->book_total)}} đ" id="txt_tongcong" disabled="disabled" class="aspNetDisabled">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

</script>


@endsection