@php
    // dd($cat_parent);  
@endphp

@extends('web.layout.master')

@section('content') 


<div class="container detail_tour">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb_t">
                <a href="/" title="trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a href="/du-lich/{{$cat_parent->id}}/{{$cat_parent->cat_slug}}.html" title="{{str_replace('tour','Du lịch',$cat_parent->cat_name) }}">{{str_replace('tour','Du lịch',$cat_parent->cat_name) }}<i class="fas fa-angle-right"></i></a>
                <a href="/du-lich-tai/{{$cate_tour->id}}/{{$cate_tour->cat_slug}}.html" title="{{$cate_tour->cat_name}}">Du lịch {{$cate_tour->cat_name}} <i class="fas fa-angle-right"></i></a>
                <a href="/chi-tiet-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html" title="{{$tour_detail->tour_title}}">Du lịch {{$tour_detail->tour_title}}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tit_detail">
                    <h4 class="title_tt">{{$tour_detail->tour_title}}</h4>
                    <span class="cbg"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2" >
                <div class="left_t_detail" id="left_info">
                    <div class="tour_price">
                        <span>Giá: </span>
                        @if($tour_detail->tour_price_pro == 0)
                        <span class="pri_real">{{$tour_detail->tour_price}} đ</span>
                        @else
                        <span class="pricetext_dis">{{$tour_detail->tour_price}} đ</span>
                        <span class="pricetext">{{$tour_detail->tour_price_pro}} đ</span>
                        @endif
                    </div>
                    <div class="tour_code">
                        <span>Mã: {{$tour_detail->tour_code}}</span>
                    </div>
                    <div class="info_tour">
                        <table cellspacing="0" cellpadding="0" border="0" class="table table-striped">
                            <tbody><tr>
                                <td>Khởi hành:</td>
                                <td>{{date("d-m-Y", strtotime($tour_detail->tour_date_go))}} </td>
                            </tr>
                            <tr>
                                <td>Ngày về:</td>
                                <td>{{date("d-m-Y", strtotime($tour_detail->tour_date_back))}}</td>
                            </tr>
                            <tr>
                                <td>Thời gian:</td>
                                <td>{{$tour_detail->tour_time}}</td>
                            </tr>
                            <tr>
                                <td>Khởi hành từ:</td>
                                <td>{{$tour_detail->tour_from}}</td>
                            </tr>
                            <tr>
                                <td>Phương tiện:</td>
                                <td>
                                    @foreach($expediency as $ex)
                                    @if($tour_detail->tour_expediency == $ex->id)
                                    <span class="depdate  pt_maybay"></span>{{$ex->exp_name}}
                                    @endif
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    <div class="tour_sp">
                        <span class="hotlinetext">HOTLINE TƯ VẤN ĐẶT TOUR</span>
                        <a href="tel:0903 178 437">
                            <i class="fas fa-mobile-alt"></i>
                            0903 178 437
                            <span class="name"> Ms. Lan</span>
                        </a>
                        <a href="tel:0903 178 437">
                            <i class="fas fa-mobile-alt"></i>
                            0903 178 437
                            <span class="name"> Ms. Hằng</span>
                        </a>
                    </div>
                    <div class="book_tour_dt">
                        <span class="tour_blank">(Số chỗ còn nhận {{$tour_detail->tour_blank}})</span>
                        @if($tour_detail->tour_blank != 0)
                        <a href="/dat-tour/{{$tour_detail->id}}-{{$tour_detail->tour_slug}}.html"><button type="button" class="btn btn-info btn_book">Đặt tour</button></a>
                        @else 
                            <button type="button" class="btn btn-danger btn_book">Hết chỗ</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="content_tour">
                    {!!$tour_detail->tour_description!!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="right_menu">
                    <div class="tit_menu">
                        <span>TOUR DU LỊCH MỚI</span>
                    </div>
                    <ul class="list_right_menu">
                        @foreach($cate_child_all as $ca)
                            <li><a href="/du-lich-tai/{{$ca->id}}/{{$ca->cat_slug}}.html">Du lịch {{$ca->cat_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @if(!empty($seen_tour))
                <div class="tour_seen">
                    <div class="tit_menu">
                        <span>TOUR ĐÃ XEM</span>
                    </div>
                    @foreach($seen_tour as $seen)
                    <div class="tour-item-daxem">
                        <div class="tour-viewed">
                            <a href="/chi-tiet-tour/{{$seen->id}}-{{$seen->tour_slug}}.html" title="{{$seen->tour_title}}">                
                                <img src="{{asset('uploadfile')}}/tour/{{$seen->tour_picture}}" title="Ảnh tour" alt="ảnh">
                                <span>{{$seen->tour_title}}</span>
                            </a>
                            <div class="schedule">
                               
                                <span class="khoihanh">Khởi hành: {{date("d-m-Y", strtotime($seen->tour_date_go))}}</span>
                                <span class="daysfix"><i class="far fa-calendar-alt"></i> {{$seen->tour_time}}</span>
                            </div>
                            <div class="tour-price">
                                @if($seen->tour_price_pro == 0)
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price}} đ</span></span>
                                @else
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price_pro}} đ</span></span>
                                <span class="prdis">
                                    <span></span>
                                    <span>{{$seen->tour_price_discount}} đ</span>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div> 
        </div>
    </div>

@endsection

@section('script')

<script>

$(document).ready(function(){

    // var flag_top = $('.left_t_detail').offset().top + 30;
    // var flag_map = $('.bottom_bg').offset().top - 100;
    // $(window).scroll(function () {
    //      if($(window).scrollTop()>flag_top){
    //         $('.left_t_detail').addClass('sticky');
    //     }else{
    //         $('.left_t_detail').removeClass('sticky');
    //     }
    //     if($(window).scrollTop()>flag_map){
    //         $('.left_t_detail').removeClass('sticky');
    //     }
    // })   
});
  
</script>


@endsection