@php
    // dd("tour");  
@endphp

@extends('web.layout.master')

@section('content') 


<div class="container list_tour">
        <div class="row">
            <div class="col-md-9">
                <div class="breadcrumb_t">
                    <a href="/" title="Trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                    <a href="/du-lich/tour-doanh-nghiep.html" title="Du lịch cho doanh nghiệp">Tour doanh nghiệp </a>
                </div>
                <div class="des_tour">
                    <h4 class="title_des">TOUR DOANH NGHIỆP</h4>
                    <span class="cbg"></span>
                    <div class="content_des">
                        <p>Với kinh nghiệm tổ chức nhiều chương trình M.I.C.E lớn (gần 2000 khách/đoàn) trong và ngoài nước, Lữ Hành Fiditour xem các cam kết về chất lượng cùng sự hài lòng của khách hàng là thước đo cho các hoạt động M.I.C.E.
                            Nhờ những nỗ lực bền bỉ trong ngành lữ hành, 15 năm liên tiếp Lữ Hành Fiditour đã vinh dự đón nhận giải thưởng uy tín nhất của ngành, Giải thưởng Du lịch Việt Nam trong cả 3 hạng mục quan trọng:
                            • Lữ hành quốc tế hàng đầu đón khách du lịch vào Việt Nam
                            • Lữ hành quốc tế hàng đầu đưa khách đi du lịch nước ngoài
                            • Lữ hành nội địa hàng đầu Việt Nam
                             
                            Lữ Hành Fiditour ghi nhận thành tích 15 năm liên tiếp đạt Giải thưởng Du lịch Việt Nam trong cả 3 hạng mục lữ hành quan trọng.
                            
                            Quý khách hãy chia sẻ với Lữ Hành Fiditour về sở thích, nhu cầu và ngân sách dành cho du lịch M.I.C.E, chúng tôi tự tin sẽ mang đến các giải pháp tốt, đáp ứng ưu việt nhất mọi mong đợi của Quý khách hàng doanh nghiệp.</p>
                           <br>
                           <p>Để đặt tour cho doanh nghiệp ( Đoàn số lượng lớn ) vui lòng liên hệ: 1800 6886</p> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="right_menu">
                    <div class="tit_menu">
                        <span>TOUR DU LỊCH MỚI</span>
                    </div>
                    <ul class="list_right_menu">
                        <li><a href="/du-lich-tai/72/nha-trang.html">Du lịch Nha Trang</a></li>
                        <li><a href="/du-lich-tai/74/mien-tay-song-nuoc.html">Du lịch Miền Tây sông nước</a></li>
                        <li><a href="/du-lich-tai/75/da-lat.html">Du lịch Đà Lạt</a></li>
                        <li><a href="/du-lich-tai/77/can-tho.html">Du lịch Cần Thơ</a></li>
                        <li><a href="/du-lich-tai/95/con-dao.html">Du lịch Côn Đảo</a></li>
                        <li><a href="/du-lich-tai/96/phu-quoc.html">Du lịch Phú Quốc</a></li>
                        <li><a href="/du-lich-tai/98/phan-thiet.html">Du lịch Phan Thiết</a></li>
                        <li><a href="/du-lich-tai/100/quy-nhon.html">Du lịch Quy Nhơn</a></li>
                    </ul>
                </div>
                @if(!empty($seen_tour))
                <div class="tour_seen">
                    <div class="tit_menu">
                        <span>TOUR ĐÃ XEM</span>
                    </div>
                    @foreach($seen_tour as $seen)
                    <div class="tour-item-daxem">
                        <div class="tour-viewed">
                            <a href="/chi-tiet-tour/{{$seen->id}}-{{$seen->tour_slug}}.html" title="{{$seen->tour_title}}">                
                                <img src="{{asset('uploadfile')}}/tour/{{$seen->tour_picture}}" title="Ảnh tour" alt="ảnh">
                                <span>{{$seen->tour_title}}</span>
                            </a>
                            <div class="schedule">
                               
                                <span class="khoihanh">Khởi hành: {{date("d-m-Y", strtotime($seen->tour_date_go))}}</span>
                                <span class="daysfix"><i class="far fa-calendar-alt"></i> {{$seen->tour_time}}</span>
                            </div>
                            <div class="tour-price">
                                @if($seen->tour_price_pro == 0)
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price}} đ</span></span>
                                @else
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price_pro}} đ</span></span>
                                <span class="prdis">
                                    <span></span>
                                    <span>{{$seen->tour_price_discount}} đ</span>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div> 
        </div>
    </div>

@endsection