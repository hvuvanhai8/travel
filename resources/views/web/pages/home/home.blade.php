@php
    // dd($tour_tn);
@endphp

@extends('web.layout.master')

@section('content')



    <div class="slide_banner_home">
        <div class="owl-carousel owl-theme single-item">
            @foreach($banner as $ban)
                <div class="item">
                    <img src="{{asset('uploadfile/banner/').'/'.$ban->ban_picture}}" alt="">
                </div>
            @endforeach
        </div>
    </div>

    <main class="home">
        <div class="container hot_tour">
            <div class="row">
                <div class="col-md-12">
                    <div class="hot_caption">
                        <div class="line_cap"></div>
                        <div class="txt_cap">
                            <span>Đi du lịch chất với</span>
                            <span>HOT TUOR</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme slide_hot_tour">
                        @foreach($tour_hot as $to)
                            <div class="item">
                                <div class="img_item">
                                    <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" title="{{$to->tour_title}}"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a>
                                    @if($to->tour_price_pro == 0)
                                        <span class="dis"></span>
                                    @else
                                        <span class="distext">{{$to->tour_price_discount}} đ</span>
                                    @endif
                                </div>
                                <div class="info_item">
                                    <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_item" title="{{$to->tour_title}}">{{$to->tour_title}}</a>
                                    <div class="date_item">
                                        <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                        <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                        <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                                    </div>
                                    <div class="price_tour">
                                        @if($to->tour_price_pro == 0)
                                            <span class="newprice">{{$to->tour_price}}đ</span>
                                        @else
                                            <strike class="oldprice">{{$to->tour_price}}đ</strike>
                                            <span class="newprice">{{$to->tour_price_pro}}đ</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="container all_tour">
            <div class="row">
                <div class="col-md-6 tn_tour">
                    <div class="tx_tour">
                        <div class="cap_tn_tour">
                            <h3><a href="/du-lich/21/tour-trong-nuoc.html" title="du lịch trong nước">Tour trong nước</a></h3>
                        </div>
                    </div>
                    <div class="main_all_tour">
                        @foreach($tour_tn as $to)
                            <div class="tour_new">
                                <div class="img_tour_al"><a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" title="{{$to->tour_title}}"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a></div>
                                <div class="info_tour_al">
                                    <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_al" title="{{$to->tour_title}}">{{$to->tour_title}}</a>
                                    <div class="date_item_al">
                                        <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                        <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                        <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                                    </div>
                                    <div class="price_tour_al">
                                        @if($to->tour_price_pro == 0)
                                            <span class="newprice">Giá: {{$to->tour_price}} đ</span>
                                        @else
                                            <span class="newprice">Giá: {{$to->tour_price_pro}} đ</span>
                                            <span class="prdis">
                                        <span></span>
                                        <span>{{$to->tour_price_discount}} đ</span>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="more_tour">
                        <a href="/du-lich/21/tour-trong-nuoc.html" title="du lịch trong nước">Xem nhiều tour hơn...</a>
                    </div>
                </div>
                <div class="col-md-6 nn_tour">
                    <div class="tx_tour">
                        <div class="cap_tn_tour">
                            <h3><a href="/du-lich/22/tour-nuoc-ngoai.html" title="du lịch nước ngoài">Tour nước ngoài</a></h3>
                        </div>
                    </div>
                    <div class="main_all_tour">
                        @foreach($tour_nn as $to)
                            <div class="tour_new">
                                <div class="img_tour_al"><a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" title="{{$to->tour_title}}"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a></div>
                                <div class="info_tour_al">
                                    <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_al" title="{{$to->tour_title}}">{{$to->tour_title}}</a>
                                    <div class="date_item_al">
                                        <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                        <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                        <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                                    </div>
                                    <div class="price_tour_al">
                                        @if($to->tour_price_pro == 0)
                                            <span class="newprice">Giá: {{$to->tour_price}} đ</span>
                                        @else
                                            <span class="newprice">Giá: {{$to->tour_price_pro}} đ</span>
                                            <span class="prdis">
                                            <span></span>
                                            <span>{{$to->tour_price_discount}} đ</span>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="more_tour">
                        <a href="/du-lich/22/tour-nuoc-ngoai.html" title="du lịch nước ngoài">Xem nhiều tour hơn...</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container banner_bw_home">
            <div class="row">
                <div class="col-md-4 img_bn_bw">
                    <img src="{{asset('web')}}/images/banner_bw/a.png" alt="banner">
                </div>
                <div class="col-md-4 img_bn_bw">
                    <img src="{{asset('web')}}/images/banner_bw/a.png" alt="banner">
                </div>
                <div class="col-md-4 img_bn_bw">
                    <img src="{{asset('web')}}/images/banner_bw/a.png" alt="banner">
                </div>
            </div>
        </div>

        <div class="container news_even">
            <div class="row">
                <div class="col-md-6 news_left">
                    <div class="cap_news">
                        <h5><a href="{{'/tin-tuc/'.$cat_news_child[1]->cat_slug}}">Tin tức & sự kiện</a></h5>
                        <span class="cbg"></span>
                    </div>
                    @if(!empty($cat_news_child[1]->first_new))
                        <div class="main_item_news">
                            <div class="img_item_news">
                                <a href="{{'tin-tuc/'.$cat_news_child[1]->first_new->new_slug}}-news{{$cat_news_child[1]->first_new->id}}.html">
                                    @if($cat_news_child[1]->first_new->new_picture !== null)
                                        <img src="{{asset('uploadfile/news').'/'.$cat_news_child[1]->first_new->new_picture}}" alt="{{$cat_news_child[1]->first_new->new_title}}">
                                        @else
                                        <img src="{{asset('web/images/default.jpg')}}">
                                    @endif
                                </a>
                            </div>
                            <div class="txt_item_news">
                                <h5><a href="{{'tin-tuc/'.$cat_news_child[1]->first_new->new_slug}}-news{{$cat_news_child[1]->first_new->id}}.html">{{$cat_news_child[1]->first_new->new_title}}</a></h5>
                                <span class="date_news"><i class="far fa-calendar-alt"></i>{{$cat_news_child[1]->first_new->created_at}}</span>
                                <div class="content_txt">
                                    {!! $cat_news_child[1]->first_new->new_teaser !!}
                                </div>
                            </div>
                        </div>
                        <div class="row news_more">
                            @foreach($cat_news_child[1]->news as $item_news)
                                <div class="col-md-6 news_b">
                                    <a href="{{'tin-tuc/'.$item_news->new_slug}}-news{{$item_news->id}}.html">
                                        @if($item_news->new_picture!==null)
                                            <img src="{{asset('uploadfile/news').'/'.$item_news->new_picture}}" alt="{{$item_news->new_title}}">
                                        @else
                                            <img src="{{asset('web/images/default.jpg')}}">
                                        @endif
                                        <span style="width: auto; font-size: 13px; max-height: 60px;  font-weight: bold;">{{$item_news->new_title}}</span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="main_item_news">
                            <h4 style="padding: 20px; width: 100%; text-align: center" >Không có tin !</h4>
                        </div>
                    @endif
                </div>
                <div class="col-md-6 news_left">
                    <div class="cap_news">
                        <h5><a href="{{'/tin-tuc/'.$cat_news_child[0]->cat_slug}}">Kinh nghiệm du lịch</a></h5>
                        <span class="cbg"></span>
                    </div>
                    @if(!empty($cat_news_child[0]->first_new))
                        <div class="main_item_news">
                            <div class="img_item_news">
                                <a href="{{'tin-tuc/'.$cat_news_child[0]->first_new->new_slug}}-news{{$cat_news_child[0]->first_new->id}}.html">
                                    @if($cat_news_child[0]->first_new->new_picture!==null)
                                        <img src="{{asset('uploadfile/news').'/'.$cat_news_child[0]->first_new->new_picture}}" alt="{{$cat_news_child[0]->first_new->new_title}}">
                                    @else
                                        <img src="{{asset('web/images/default.jpg')}}">
                                    @endif
                                </a>
                            </div>
                            <div class="txt_item_news">
                                <h5><a href="{{'tin-tuc/'.$cat_news_child[0]->first_new->new_slug}}-news{{$cat_news_child[0]->first_new->id}}.html">{{$cat_news_child[0]->first_new->new_title}}</a></h5>
                                <span class="date_news"><i class="far fa-calendar-alt"></i>{{$cat_news_child[0]->first_new->created_at}}</span>
                                <div class="content_txt">
                                    {!! $cat_news_child[0]->first_new->new_teaser !!}
                                </div>
                            </div>
                        </div>
                        <div class="row news_more">
                            @foreach($cat_news_child[0]->news as $item_news)
                                @if($item_news->new_cat_id==$cat_news_child[0]->id)
                                    <div class="col-md-3 news_c">
                                        <a href="{{'tin-tuc/'.$item_news->new_slug}}-news{{$item_news->id}}.html">
                                            @if($item_news->new_picture!==null)
                                                <img src="{{asset('uploadfile/news').'/'.$item_news->new_picture}}" alt="">
                                            @else
                                                <img src="{{asset('web/images/default.jpg')}}">
                                            @endif
                                            <span style="width: auto; font-size: 13px; max-height: 60px; font-weight: bold;">{{$item_news->new_title}}</span>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @else
                        <div class="main_item_news">
                            <h4 style="padding: 20px; width: 100%; text-align: center" >Không có tin !</h4>
                        </div>
                    @endif
                </div>
            </div>
        </div>




    </main>

@endsection


