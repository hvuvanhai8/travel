@php
// $place = current($place)
@endphp

@extends('web.layout.master')

@section('content') 

<div class="container list_tour">
    <div class="row">
        <div class="col-md-9">
            <div class="breadcrumb_t">
                <a href="/"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a href="/du-lich/{{$cat_parent->id}}/{{$cat_parent->cat_slug}}.html" title="{{str_replace('tour','Du lịch',$cat_parent->cat_name) }}">{{str_replace('tour','Du lịch',$cat_parent->cat_name) }}<i class="fas fa-angle-right"></i></a>
                <a href="/du-lich-tai/{{$cat_parent_id->id}}/{{$cat_parent_id->cat_slug}}.html" title="Du lịch {{$cat_parent_id->cat_name}}">Du lịch {{$cat_parent_id->cat_name}}</a>
            </div>
            <div class="des_tour">
            <h4 class="title_des">TOUR {{$place_list->pla_name}} - DU LỊCH {{$place_list->pla_name}}</h4>
                <span class="cbg"></span>
                <div class="content_des">
                <p>{!!$place_list->pla_description!!}</p>
                </div>
            </div>
            <div class="main_list_tour">
                <div class="filter_tour">
                    <span class="count_t">Có tổng cộng {{count($tour)}} tour</span>
                    <span><i class="fas fa-filter"></i> Sắp xếp theo</span>
                    <select class="form-control fil_list">
                        <option value="0">Mặc định</option>
                        <option>Giá giảm dần</option>
                        <option>Giá tăng dần</option>
                      </select>
                </div>
                <div class="all_tour_list">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col" class="tour_h">Tour</th>
                            <th scope="col">Khởi hành</th>
                            <th scope="col">Số ngày đi</th>
                            <th scope="col">Giá ( đồng )</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($tour as $to)
                          <tr>
                            <td class="td_img">
                                <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="item_list_t">
                                    <div class="img_list_t"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt=""></div>
                                    <div class="tit_list_t"><img src="{{asset('web')}}/images/hotimg.gif" alt="hot">{{$to->tour_title}}</div>
                                </a>
                                <div class="code_t">
                                    <span>Mã tour: {{$to->tour_code}}</span> 
                                </div>
                            </td>
                            <td>{{date("d-m-Y", strtotime($to->tour_date_go))}}</td>
                            <td>{{$to->tour_time}}</td>
                            <td class="pri_tour">
                                @if($to->tour_price_pro == 0)
                                <p><span class="new_pri_n">{{$to->tour_price}}đ</span></p>
                                @else
                                <p><strike class="oldprice">{{$to->tour_price}}đ</strike></p>
                                <p><span class="new_pri">{{$to->tour_price_pro}}đ</span></p>
                                @endif
                            </td>
                            @if($to->tour_blank != 0)
                                <td class="bok_tour"><a href="/dat-tour/{{$to->id}}-{{$to->tour_slug}}.html"><button type="button" class="btn btn-primary btn_book">Đặt tour</button></a></td>
                            @else
                                <td class="bok_tour"><button type="button" class="btn btn-danger btn_book" style="color: #fff;">Hết chỗ</button></td>
                            @endif
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                </div>
                <div class="all_tour_list_mb">
                    @foreach($tour as $to)
                    <div class="tour_new_cat">
                        <div class="img_tour_al"><a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a></div>
                        <div class="info_tour_al">
                            <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_al">{{$to->tour_title}}</a>
                            <div class="date_item_al">
                                <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                            </div>
                            <div class="price_tour_al">
                                @if($to->tour_price_pro == 0)
                                    <span class="newprice">Giá: {{$to->tour_price}} đ</span>
                                @else
                                    <span class="newprice">Giá: {{$to->tour_price_pro}} đ</span>
                                    <span class="prdis">
                                        <span></span>
                                        <span>{{$to->tour_price_discount}} đ</span>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="right_menu">
                <div class="tit_menu">
                    <span>TOUR DU LỊCH MỚI</span>
                </div>
                <ul class="list_right_menu">
                    @foreach($cate_right as $ca)
                        <li><a href="/du-lich-tai/{{$ca->id}}/{{$ca->cat_slug}}.html">Du lịch {{$ca->cat_name}}</a></li>
                    @endforeach
                </ul>
            </div>
            @if(!empty($seen_tour))
            <div class="tour_seen">
                <div class="tit_menu">
                    <span>TOUR ĐÃ XEM</span>
                </div>
                @foreach($seen_tour as $seen)
                <div class="tour-item-daxem">
                    <div class="tour-viewed">
                        <a href="/chi-tiet-tour/{{$seen->id}}-{{$seen->tour_slug}}.html" title="{{$seen->tour_title}}">                
                            <img src="{{asset('uploadfile')}}/tour/{{$seen->tour_picture}}" title="Ảnh tour" alt="ảnh">
                            <span>{{$seen->tour_title}}</span>
                        </a>
                        <div class="schedule">
                           
                            <span class="khoihanh">Khởi hành: {{date("d-m-Y", strtotime($seen->tour_date_go))}}</span>
                            <span class="daysfix"><i class="far fa-calendar-alt"></i> {{$seen->tour_time}}</span>
                        </div>
                        <div class="tour-price">
                            @if($seen->tour_price_pro == 0)
                            <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price}} đ</span></span>
                            @else
                            <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price_pro}} đ</span></span>
                            <span class="prdis">
                                <span></span>
                                <span>{{$seen->tour_price_discount}} đ</span>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div> 
    </div>
</div>

@endsection