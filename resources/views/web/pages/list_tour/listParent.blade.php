@php
    // dd("tour");  
@endphp

@extends('web.layout.master')

@section('content') 


<div class="container list_tour">
        <div class="row">
            <div class="col-md-9">
                <div class="breadcrumb_t">
                    <a href="/" title="Trang chủ"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                    @if($cat_type == 'trongnuoc')
                    <a href="/du-lich/{{$cat_parent->id}}/{{$cat_parent->cat_slug}}.html" title="Du lịch trong nước">Du lịch trong nước </a>
                    @elseif($cat_type == 'nuocngoai')
                    <a href="/du-lich/{{$cat_parent->id}}/{{$cat_parent->cat_slug}}.html" title="Du lịch nước ngoài">Du lịch nước ngoài </a>
                    @endif
                </div>
                @if($cat_type == 'trongnuoc')
                <div class="des_tour">
                    <h4 class="title_des">DU LỊCH TRONG NƯỚC</h4>
                    <span class="cbg"></span>
                    <div class="content_des">
                        <p>Tổng hợp chương trình tour du lịch trong nước với những điểm đến mới rất hấp dẫn. Chương trình du lịch Việt Nam 2019 tại Lữ Hành Fiditour đã được chọn lọc, khảo sát kỹ càng nhằm đem đến Quý Khách những trải nghiệm mới lạ, thú vị nhất. Lữ Hành Fiditour tự hào với chất lượng tiên phong cũng như vinh dự là doanh nghiệp lữ hành với 19 năm liên tiếp được vinh danh là "Top 10 doanh nghiệp kinh doanh lữ hành nội địa hàng đầu Việt Nam”. Hãy cùng chúng tôi khám phá những điểm đến hấp dẫn thu hút khách du lịch qua các tour trong nước như: du lịch Côn Đảo, du lịch Phú Quốc, du lịch Đà Lạt, du lịch Nha Trang, du lịch Hạ Long, du lịch Sapa, du lịch Đà Nẵng, du lịch Miền Tây,....</p>
                    </div>
                </div>
                @elseif($cat_type == 'nuocngoai')
                <div class="des_tour">
                        <h4 class="title_des">DU LỊCH NƯỚC NGOÀI</h4>
                        <span class="cbg"></span>
                        <div class="content_des">
                            <p>Lữ hành Fiditour luôn là bạn đồng hành số 1 với du khách trên mọi nẻo đường du lịch nước ngoài. Với gần 30 năm kinh nghiệm trong lĩnh vực du lịch lữ hành, hơn 1000 tuyến du lịch khắp 5 châu mỗi năm. Đến với Lữ Hành Fiditour bạn sẽ tự tin khám phá và trải nghiệm được nhiều hơn, ở những vùng đất mới từ các quốc gia Đông Nam Á, xa hơn là các nước ở Châu Âu, Châu Úc, Châu Mỹ và thú vị hơn cả là vùng đất Châu Phi xa tít...  Đặc biệt, các tour du lịch nước ngoài 2019 với nhiều chương trình khuyến mãi hấp dẫn và phần quà có giá trị từ Lữ Hành Fiditour đang chờ quý khách hàng: du lịch Đài Loan, du lịch Hàn Quốc, du lịch Nhật Bản, du lịch Campuchia, du lịch Singapore, du lịch Malaysia, du lịch Mỹ, du lịch Pháp, du lịch Nga,...</p>
                        </div>
                    </div>
                @endif
                <div class="main_list_tour">
                    <div class="filter_tour">
                        <span class="count_t">Có tổng cộng {{count($tour_tn)}} tour</span>
                        <span><i class="fas fa-filter"></i> Sắp xếp theo</span>
                        <select class="form-control fil_list">
                            <option value="0">Mặc định</option>
                            <option>Giá giảm dần</option>
                            <option>Giá tăng dần</option>
                          </select>
                    </div>
                    <div class="all_tour_list">
                            @foreach($tour_tn as $to)
                            <div class="tour_new_cat">
                                <div class="img_tour_al"><a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a></div>
                                <div class="info_tour_al">
                                    <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_al">{{$to->tour_title}}</a>
                                    <div class="date_item_al">
                                        <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                        <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                        <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                                    </div>
                                    <div class="price_tour_al">
                                        @if($to->tour_price_pro == 0)
                                            <span class="newprice">Giá: {{$to->tour_price}} đ</span>
                                        @else
                                            <span class="newprice">Giá: {{$to->tour_price_pro}} đ</span>
                                            <span class="prdis">
                                                <span></span>
                                                <span>{{$to->tour_price_discount}} đ</span>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="right_menu">
                    <div class="tit_menu">
                        <span>TOUR DU LỊCH MỚI</span>
                    </div>
                    <ul class="list_right_menu">
                        @foreach($cate_child as $ca)
                            <li><a href="/du-lich-tai/{{$ca->id}}/{{$ca->cat_slug}}.html">Du lịch {{$ca->cat_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @if(!empty($seen_tour))
                <div class="tour_seen">
                    <div class="tit_menu">
                        <span>TOUR ĐÃ XEM</span>
                    </div>
                    @foreach($seen_tour as $seen)
                    <div class="tour-item-daxem">
                        <div class="tour-viewed">
                            <a href="/chi-tiet-tour/{{$seen->id}}-{{$seen->tour_slug}}.html" title="{{$seen->tour_title}}">                
                                <img src="{{asset('uploadfile')}}/tour/{{$seen->tour_picture}}" title="Ảnh tour" alt="ảnh">
                                <span>{{$seen->tour_title}}</span>
                            </a>
                            <div class="schedule">
                               
                                <span class="khoihanh">Khởi hành: {{date("d-m-Y", strtotime($seen->tour_date_go))}}</span>
                                <span class="daysfix"><i class="far fa-calendar-alt"></i> {{$seen->tour_time}}</span>
                            </div>
                            <div class="tour-price">
                                @if($seen->tour_price_pro == 0)
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price}} đ</span></span>
                                @else
                                <span class="prtext">Giá: <span class="oldprice">{{$seen->tour_price_pro}} đ</span></span>
                                <span class="prdis">
                                    <span></span>
                                    <span>{{$seen->tour_price_discount}} đ</span>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div> 
        </div>
    </div>

@endsection