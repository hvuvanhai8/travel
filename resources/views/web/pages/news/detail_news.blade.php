@php
    // dd($tour);
@endphp

@extends('web.layout.master')

@section('content')


    <div class="container list_news">
        <div class="col-md-9 col-sm-9 col-xs-12 left-col">
            <div class="article-panel">
                <div class="cate-caption">
                    <h1>{{$category->cat_name}}</h1>
                </div>
                <article class="post">
                    <header>
                        <h1><b>{{$news->new_title}}</b></h1>
                        <div class="update-time">
                            <span class="updtext">{{$news->created_at}}</span>
                        </div>
                    </header>
                    <div class="entry-content">
                        <div class="share-blog">

                        </div>
                        <div class="entry-main">
                            <div class="content-brief">
                                <div class="img_detail">
                                    <img src="{{asset('uploadfile/news').'/'.$news->new_picture}}"
                                         alt="{{$news->new_title}}">
                                </div>
                                <div class="article-brief">
                                    <p>
                                        <em>
                                            {!!  $news->new_teaser !!}
                                        </em>
                                    </p>
                                </div>
                            </div>
                            <div id="articlebody" class="article-body">
                                {!! $news->new_description !!}
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>

@endsection