@php
    // dd($tour);
@endphp

@extends('web.layout.master')

@section('content')


    <div class="container list_news">
        <div class="col-md-9 col-sm-9 col-xs-12 left-col">
            <div class="article-panel">
                <div class="cate-caption">
                    <h1>{{$category->cat_name}}</h1>

                </div>
                @if($news !== null)
                    <div style="display: inline-block; width:100%;">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 latest-article">
                                <article>
                                    <a href="{{$news_new->new_slug}}-news{{$news_new->id}}.html" alt="{{$news_new->new_title}}">
                                        <img src="{{asset('uploadfile/news').'/'.$news_new->new_picture}}">
                                    </a>
                                    <div class="content_news">
                                        <a class="news_title" href="{{$news_new->new_slug}}-news{{$news_new->id}}.html"
                                           title="{{$news_new->new_title}}" alt="{{$news_new->new_title}}">
                                            {{$news_new->new_title}} </a>
                                        <span class="datepost">{{$news_new->created_at}}</span>
                                        <p>{!! $news_new->new_teaser !!}</p>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 latest-relate">
                                @foreach($news_latest_relate as $item)
                                    <article>
                                        <a href="{{$news_new->new_slug}}-news{{$item->id}}.html" alt="{{$news_new->new_title}}">
                                            <img src="{{asset('uploadfile/news').'/'.$item->new_picture}}"
                                                 alt="{{$item->new_title}}">
                                        </a>
                                        <div class="content_news">
                                            <a class="news_title" href="{{$item->new_slug}}-news{{$item->id}}.html">
                                                {{$item->new_title}}
                                            </a>
                                            <span class="datepost">{{$item->created_at}}</span>
                                            <span class="news_teaser">{!! $item->new_teaser !!}</span>
                                        </div>
                                    </article>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="postlist">
                        <div class="row">
                            @foreach($news as $item)
                                <div class="col-md-4 col-sm-4 col-xs-12 post-item first-post">
                                    <article>
                                        <a href="{{$item->new_slug}}.html" alt="{{$item->new_title}}">
                                            <img src="{{asset('uploadfile/news').'/'.$item->new_picture}}"
                                                 alt="{{$item->new_title}}">
                                        </a>
                                        <div class="content_news">
                                            <a class="news_title" href="{{$item->new_slug}}.html">
                                                {{$item->new_title}}
                                            </a>
                                            <span class="datepost">{{$item->created_at}}</span>
                                            <span class="news_teaser">{!! $item->new_teaser !!}</span>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="pagerdiv">
                        <div class="paginate_cate text-center">
                            {{$news->links()}}
                        </div>
                    </div>
                    @else
                    <div class="no_news">
                        <h2>Không tìm thấy tin nào.</h2>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection