@php
    // dd($tour);   
@endphp

@extends('web.layout.master')

@section('content') 


<div class="container search_tour">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb_t">
                <a href="/"><i class="fas fa-home"></i> Trang chủ <i class="fas fa-angle-right"></i></a>
                <a title="tìm kiếm"> Tìm Kiếm</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="keyword_t">
                <h5>KẾT QUẢ TÌM KIẾM</h5>
                <span>CHO TỪ KHÓA: {{$keyword}}</span>
                <p class="cout_tour">Có tổng cộng {{count($tour)}} tour</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main_list_tour">
                <div class="all_tour_list">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col" class="tour_h">Tour</th>
                            <th scope="col">Khởi hành</th>
                            <th scope="col">Số ngày đi</th>
                            <th scope="col">Giá ( đồng )</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($tour as $to)
                          <tr>
                            <td class="td_img">
                                <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="item_list_t">
                                    <div class="img_list_t"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt=""></div>
                                    <div class="tit_list_t"><img src="{{asset('web')}}/images/hotimg.gif" alt="">{{$to->tour_title}}</div>
                                </a>
                                <div class="code_t">
                                    <span>Mã tour: {{$to->tour_code}}</span> 
                                </div>
                            </td>
                            <td>{{date("d-m-Y", strtotime($to->tour_date_go))}}</td>
                            <td>{{$to->tour_time}}</td>
                            <td class="pri_tour">
                                @if($to->tour_price_pro == 0)
                                <p><span class="new_pri_n">{{$to->tour_price}}đ</span></p>
                                @else
                                <p><strike class="oldprice">{{$to->tour_price}}đ</strike></p>
                                <p><span class="new_pri">{{$to->tour_price_pro}}đ</span></p>
                                @endif
                            </td>
                            @if($to->tour_blank != 0)
                                <td class="bok_tour"><a href="/dat-tour/{{$to->id}}-{{$to->tour_slug}}.html"><button type="button" class="btn btn-primary btn_book">Đặt tour</button></a></td>
                            @else
                                <td class="bok_tour"><button type="button" class="btn btn-danger btn_book" style="color: #fff;">Hết chỗ</button></td>
                            @endif
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                </div>
                <div class="all_tour_list_mb">
                    @foreach($tour as $to)
                    <div class="tour_new_cat">
                        <div class="img_tour_al"><a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html"><img src="{{asset('uploadfile')}}/tour/{{$to->tour_picture}}" alt="ảnh"></a></div>
                        <div class="info_tour_al">
                            <a href="/chi-tiet-tour/{{$to->id}}-{{$to->tour_slug}}.html" class="title_al">{{$to->tour_title}}</a>
                            <div class="date_item_al">
                                <span>Khởi hành <i class="fas fa-plane-departure"></i></span>
                                <span class="date_tour">{{date("d-m-Y", strtotime($to->tour_date_go))}}</span>
                                <span class="day_tuor"><i class="far fa-calendar-alt"></i> {{$to->tour_time}}</span>
                            </div>
                            <div class="price_tour_al">
                                @if($to->tour_price_pro == 0)
                                    <span class="newprice">Giá: {{$to->tour_price}} đ</span>
                                @else
                                    <span class="newprice">Giá: {{$to->tour_price_pro}} đ</span>
                                    <span class="prdis">
                                        <span></span>
                                        <span>{{$to->tour_price_discount}} đ</span>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 align_right">
            {{-- {{$tour->links()}} --}}
        </div>
    </div>
</div>

@endsection