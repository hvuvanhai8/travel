<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('foo', function () {
    return 'Hello World';
});

Route::group(['prefix'=>'admin/login','middleware'=>'CheckLogin'],function(){
    Route::get('/', 'Admin\LoginController@getLogin')->name('admin.login');
    Route::post('/', 'Admin\LoginController@postLogin')->name('admin.post.login');
});

Route::group(['prefix'=>'admin','middleware'=>'CheckAdmin'],function(){
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::resource('place', 'Admin\PlaceController');
    // Route tour
    Route::resource('tour', 'Admin\TourController');

    Route::post('/tour/update/{id}','Admin\TourController@updateTour');
    Route::get('/tour/delete/{id}','Admin\TourController@deleteTour');
    
    Route::resource('news', 'Admin\NewsController');

    // Route danh mục cha
    Route::resource('category', 'Admin\CategoryController');
    Route::resource('banner', 'Admin\BannerController');
    Route::get('/delete/{id}','Admin\CategoryController@delete');
    // Route danh mục con
    Route::get('/child/category/{id}','Admin\CategoryController@indexCateChild')->name('catechild.index');
    Route::get('/child/category/add/{id}','Admin\CategoryController@createCateChild');
    Route::post('/child/category/add/{id}','Admin\CategoryController@storeCateChild');
    Route::get('/child/category/edit/{id}','Admin\CategoryController@showCateChild');
    Route::post('/child/category/edit/{id}','Admin\CategoryController@editCateChild');
    Route::get('/child/category/delete/{id}','Admin\CategoryController@deleteCateChild');

    //route quan ly dat hang
    Route::get('/booking', 'Admin\BookingController@getBooking')->name('book.index');
    Route::get('/booking/success', 'Admin\BookingController@getBookingSuccess')->name('book.success');
    Route::get('/booking/{id}', 'Admin\BookingController@getdetailBooking')->name('book.detail');
    Route::get('/booking/success/change/{id}/{status}', 'Admin\BookingController@getChangeBookingsuccess')->name('book.change');

    Route::get('getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');
});



// Route::resource('admin/place', 'Admin\PlaceController');
// Route::resource('/tour', 'Admin\TourController');



//route frontend

Route::group(['middleware'=>'compressHTML'],function(){
    Route::get('/', 'Web\HomeController@getHome')->name('home.index');
    Route::get('/test', 'Web\HomeController@getTest');

    Route::get('/du-lich/{id}/{slug}.html', 'Web\HomeController@getCategoryParent');
    Route::get('/du-lich/tour-doanh-nghiep.html', 'Web\HomeController@getTourDoanhNghiep');
    Route::get('/du-lich-tai/{id}/{slug}.html', 'Web\HomeController@getCategoryChild');
    Route::get('/chi-tiet-tour/{id}-{slug}.html', 'Web\HomeController@getTourdetail');
    Route::post('/tim-kiem.html', 'Web\HomeController@getSearchTour')->name('seach.tour');
    Route::get('/ajax/loaitin/{id}', 'Web\HomeController@getFilterTour')->name('ajax.filter');
    Route::post('/loc-tour', 'Web\HomeController@getFilterTourHome')->name('filter.tour');

    //route booking
    Route::get('/dat-tour/{id}-{slug}.html', 'Web\BookController@getBooking');

    Route::get('/tin-tuc/{slug_detail}-news{id}.html', 'Web\NewsController@detail')->where([
        'slug_detail'=>'[^/]*',
        'id'=>'[^/]*'
    ]);
    Route::get('/tin-tuc/{slug}', 'Web\NewsController@index');
    Route::post('/dat-tour/{id}-{slug}.html', 'Web\BookController@postBooking1');

    Route::post('/dat-tour/user={id_user}&tong={total}/{id_book}/{id}-{slug}.html', 'Web\BookController@postBooking2');
    Route::get('/dat-tour/{id_user}/{id_tour}/{id_book}-bookview.html', 'Web\BookController@getviewBooking2')->name('bookview');

    Route::post('/confirm/{id_book}', 'Web\BookController@postConfirm1');
    Route::get('/confirm/thanhcong-{id_book}.html', 'Web\BookController@postConfirm2')->name('confirm2');
});